import 'package:blog/config/config.dart';
import 'package:blog/models/common/language-config.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';

class ConfigService {

  static Future<List<LanguageConfig>> getConfig() async {
    try {
      final response = await http.get('${EnvConfig.API_URL}/language/config');
      ResponseData responseData = response.data;
      List<dynamic> list = List.from(responseData.data);
      List<LanguageConfig> configs = list.map((config) => LanguageConfig.fromJson(config)).toList();
      return configs;
    } on DioError catch (e) {
      print(' ERROR : ${e.toString()}');
      return [];
    }
  }

  static Future<Map<String, dynamic>> getLanguages(String languageCode) async {
    try {
      String url = '${EnvConfig.API_URL}/language/$languageCode';
      final response = await http.get(url);
      ResponseData responseData = response.data;
      Map<String, dynamic> _result = responseData.data;
      return _result;
    } on DioError catch (e) {
      print(' ERROR : ${e.toString()}');
      return {};
    }
  }

}
