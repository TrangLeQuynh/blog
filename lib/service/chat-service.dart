import 'package:blog/config/config.dart';
import 'package:blog/models/chat/chat-message.dart';
import 'package:blog/models/chat/chat-room.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class ChatService {

  static Future<ResponseData> getListMessage({
    @required num sender,
  }) async {
    List<ChatMessage> li = [];
    try {
      String url = '${EnvConfig.API_URL}/messages/$sender';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data?.length ?? 0; ++i) {
        li.add(
          ChatMessage.fromJson(data[i])
        );
      }
      return new ResponseData(true, responseData.message, li, null);
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }

  static Future<ResponseData> getListChatRoom() async {
    List<ChatRoom> li = [];
    try {
      String url = '${EnvConfig.API_URL}/chat-room/all';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data?.length ?? 0; ++i) {
        li.add(ChatRoom.fromJson(data[i]));
      }
      return new ResponseData(true, responseData.message, li, null);
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', li, null);
    }
  }

  static Future<void> seeMessage(num id) async {
    try {
      String url = '${EnvConfig.API_URL}/seen-message/$id';
      Response response = await http.get(url);
    } catch (e) {
      print(e.toString());
    }
  }

}
