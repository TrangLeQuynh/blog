import 'dart:convert';

import 'package:blog/models/common/payload.dart';
import 'package:blog/plugin/locator.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/screens/posts/post-detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final NavigatorService _navigationService = locator<NavigatorService>();

class NotificationService {

  static final NotificationService _notificationService = NotificationService._internal();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  Future<void> init() async {
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    final AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    final MacOSInitializationSettings initializationSettingsMacOS = MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: selectNotification
    );
  }

  Future<void> showNotification({
    @required String title,
    @required String subtitle,
    @required String payload,
  }) async {
    const IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );
    const AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'id',
      'name',
      'description',
      icon: 'ic_launcher',
      importance: Importance.max,
      priority: Priority.high,
      showWhen: false,
    );
    const NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iosNotificationDetails,
    );
    print('to show notification');
    await flutterLocalNotificationsPlugin.show(
      0,
      '$title',
      '$subtitle',
      platformChannelSpecifics,
        payload: payload,
    );
  }


  Future selectNotification(String payload) async {
    //Handle notification tapped logic here
    Map<String, dynamic> result = json.decode(payload);
    Payload _payload = Payload.fromJson(result);
    if (_payload.type == 1) {
      _navigationService.pushNamedAndRemoveUntil(RoutingNameConstant.homeScreen, arguments: 2,);
    } else {
      _navigationService.pushNamedAndRemoveUntil(RoutingNameConstant.homeScreen, arguments: 0,);
      _navigationService.push(
        MaterialPageRoute(
          builder: (c) => PostDetailScreen(postId: _payload.id,)
        )
      );
    }

  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
  }
}
