import 'package:blog/config/config.dart';
import 'package:blog/models/common/token.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:dio/dio.dart';

class AuthService {

  static Future<ResponseData> getToken(Map<String, String> formData) async {
    try {
      SharedPrefsService.removeToken();
      String url = '${EnvConfig.API_URL}/auth/login';
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      if (responseData.status) {
        Token token = Token.fromJson(responseData.data);
        SharedPrefsService.setToken(token.accessToken);
      }
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> sendVerify(String email) async {
    try {
      print('resend');
      String url = '${EnvConfig.API_URL}/register/send-verify';
      Response response = await http.post(url, data: { 'email' : email,});
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> register(Map<String, dynamic> formData) async {
    try {
      String url = '${EnvConfig.API_URL}/register';
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> confirmVerifyRegister(Map<String, dynamic> formData) async {
    try {
      String url = '${EnvConfig.API_URL}/register/confirm-verify';
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

}