import 'package:blog/config/config.dart';
import 'package:blog/models/post/post.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';

class PostService {


  static Future<ResponseData> getListPost(Map<String,dynamic> param, { num userId }) async {
    List<Post> li = [];
    try {
      String url;
      if (userId == null) {
        url = '${EnvConfig.API_URL}/post/list-post-all';
      } else {
        url = '${EnvConfig.API_URL}/post/list-post/$userId';
      }
      Response response = await http.get(url, queryParameters: param);
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data.length ; ++i) {
        li.add(Post.fromJson(data[i]));
      }
      return new ResponseData(true, responseData.message, li, null);
    } on Exception catch(e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', li, null);
    }
  }

  static Future<ResponseData> editPost(Map<String, dynamic> formData, bool canEdit) async {
    try {
      String url;
      if (canEdit) {
        url = '${EnvConfig.API_URL}/post/update';
      } else {
        url = '${EnvConfig.API_URL}/post/add';
      }
      print('call api');
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      print(responseData.toString());
      return responseData;
    } on Exception catch(e) {
      print(e.toString());
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }

  static Future<ResponseData> getPostDetail(num id) async {
    try {
      String url = '${EnvConfig.API_URL}/post/$id';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      if (responseData.status) {
        Post post = Post.fromJson(responseData.data);
        return new ResponseData(responseData.status, responseData.message, post, null);
      }
      return new ResponseData(responseData.status, responseData.message, null, null);
    } on Exception catch(e) {
      print(e.toString());
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> deletePost(num id) async {
    try {
      String url = '${EnvConfig.API_URL}/post/delete/$id';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      return responseData;
    } on Exception catch(e) {
      print(e.toString());
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }


}