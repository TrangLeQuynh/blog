import 'package:blog/config/config.dart';
import 'package:blog/models/post/comment.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';

class CommentService {

  static Future<ResponseData> addComment(Map<String, dynamic> formData) async {
    try {
      String url = '${EnvConfig.API_URL}/comment/add';
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> deleteComment(num id) async {
    try {
      String url = '${EnvConfig.API_URL}/comment/delete/$id';
      Response response = await http.post(url);
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> getListComment(num postId) async {
    List<Comment> li = [];
    try {
      String url = '${EnvConfig.API_URL}/comment/$postId';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data?.length ?? 0; ++i) {
        li.add(
          Comment.fromJson(data[i])
        );
      }
      return new ResponseData(true, responseData.message, li, null);
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }
}