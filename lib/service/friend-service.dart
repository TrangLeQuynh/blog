import 'package:blog/config/config.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/models/user/friend.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';

class FriendService {

  static Future<ResponseData> getRequestList() async {
    List<Friend> li = [];
    try {
      String url = '${EnvConfig.API_URL}/friend/request-list';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data?.length ?? 0; ++i) {
        li.add(Friend.fromJson(data[i]));
      }
      return new ResponseData(true, responseData.message, li, null);
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }

  static Future<ResponseData> getFriendList(String textSearch) async {
    List<Friend> li = [];
    try {
      String url = '${EnvConfig.API_URL}/friend/friend-list';
      Response response = await http.get(url, queryParameters: {'search' : textSearch});
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data?.length ?? 0; ++i) {
        li.add(Friend.fromJson(data[i]));
      }
      return new ResponseData(true, responseData.message, li, null);
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }

  static Future<ResponseData> findNewFriendList(String textSearch) async {
    if (textSearch.trim().length == 0) {
      return new ResponseData(true, 'null', null, null);
    }
    List<User> li = [];
    try {
      String url = '${EnvConfig.API_URL}/user/find-new-friend';
      Response response = await http.get(url, queryParameters: {'search' : textSearch});
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data?.length ?? 0; ++i) {
        li.add(User.fromJson(data[i]));
      }
      return new ResponseData(true, responseData.message, li, null);
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }

  static Future<ResponseData> responseFriend(Friend item, bool status) async {
    try {
      String url = '${EnvConfig.API_URL}/friend/response-friend';
      Map<String, dynamic> formData = {
        'userId' : item.user.id,
        'friendId' : item.friend.id,
        'status' : status
      };
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> addFriend(Map<String, dynamic> formData) async {
    try {
      String url = '${EnvConfig.API_URL}/friend/add-friend';
      Response response = await http.post(url, data: formData);
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }


}