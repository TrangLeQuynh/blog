import 'package:blog/config/config.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';

class ImageService {

  static Future<ResponseData> getListImage(Map<String, dynamic> param) async {
    List<String> li = [];
    String url = '${EnvConfig.API_URL}/images';
    print('param[page] = ${param['page']}');
    try {
      Response response = await http.get(url, queryParameters: param);
      ResponseData responseData = response.data;
      List<dynamic> data = responseData.data as List;
      for(int i = 0; i < data.length ; ++i) {
        li.add(data[i]);
      }
      return new ResponseData(true, responseData.message, li, null);
    } on Exception catch(e) {
      print(e.toString());
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', [], null);
    }
  }

}
