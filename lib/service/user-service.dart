import 'package:blog/config/config.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/models/post/image-file.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/dio.dart';
import 'package:dio/dio.dart';

class UserService {

  static Future<ResponseData> changePassword(String newPassword) async {
    try {
      print('change password');
      String url = '${EnvConfig.API_URL}/user/change-password';
      Response response = await http.post(url, data: { 'new_password' : newPassword,});
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<User> getUserInfo() async {
    try {
      String url = '${EnvConfig.API_URL}/user/info';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      print(responseData.toJson());
      if (responseData.data != null) {
        User user = User.fromJson(responseData.data);
        return user;
      }
      return null;
    } catch (e) {
      return null;
    }
  }

  static Future<num> accountFriends(num userId) async {
    try {
      String url = '${EnvConfig.API_URL}/friend/account-friends/$userId';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      return responseData.data;
    } catch (e) {
      return 0;
    }
  }

  static Future<num> accountPosts(num userId) async {
    try {
      String url = '${EnvConfig.API_URL}/post/account-posts/$userId';
      Response response = await http.get(url);
      ResponseData responseData = response.data;
      return responseData.data;
    } catch (e) {
      return 0;
    }
  }

  static Future<ResponseData> updateAvatar(String avatar) async {
    try {
      String url = '${EnvConfig.API_URL}/user/change-avatar';
      Response response = await http.post(url, data: {
        'avatar' : avatar,
      });
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

  static Future<ResponseData> updateCover(String cover) async {
    try {
      String url = '${EnvConfig.API_URL}/user/change-cover';
      Response response = await http.post(url, data: {
        'cover' : cover,
      });
      ResponseData responseData = response.data;
      return responseData;
    } catch (e) {
      return new ResponseData(false, 'MSG_SYSTEM_SOME_ERROR', null, null);
    }
  }

}