import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class CommonStyles {

  static BoxShadow getShadow(BuildContext context) => BoxShadow(
    color: Theme.of(context).shadowColor,
    offset: Offset(0.5,0.5),
    spreadRadius: 0,
    blurRadius: 6,
  );

}
