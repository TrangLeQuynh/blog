abstract class RoutingNameConstant {
  static const String splashScreen = '/splash';
  static const String loginScreen = '/login';
  static const String registerScreen = '/register';
  static const String verifyScreen = '/register/verify-code';
  static const String homeScreen = '/home';
  static const String postEditionScreen = '/post/edit';
  static const String postDetailScreen = 'post/detail';
}
