import 'package:blog/router/routing_name.dart';
import 'package:blog/screens/Login/login.dart';
import 'package:blog/screens/layout/layout.dart';
import 'package:blog/screens/login/register/register.dart';
import 'package:blog/screens/login/register/verification-otp.dart';
import 'package:blog/screens/posts/post-edition.dart';
import 'package:blog/screens/splash/splash.dart';
import 'package:flutter/cupertino.dart';

abstract class RouterConstant {

  static final routes = <String, WidgetBuilder> {
    RoutingNameConstant.splashScreen : (_) => new SplashScreen(),
    RoutingNameConstant.loginScreen : (_) => new LoginScreen(),
    RoutingNameConstant.registerScreen: (_) => new RegisterScreen(),
    RoutingNameConstant.homeScreen: (_) => new DashboardHomePage(),
    RoutingNameConstant.postEditionScreen: (_) => new PostEditionScreen(),
    RoutingNameConstant.verifyScreen: (_) => new Verification(),
  };

}