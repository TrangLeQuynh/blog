import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:blog/styles/colors.dart';

enum AppTheme { Light, Dark }

final appThemeData = {
  AppTheme.Light: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.white,
    primaryColorDark: Color(0xFF02B2B9),
    scaffoldBackgroundColor: Color(0xFFF6F6F6),
    disabledColor: Color(0xFF9EA5A3),
    //scaffoldBackgroundColor: Color(0xFFD6F1F2),
    accentColor: Color(0xFFFFCF00),
    cursorColor: Color(0xFF02B2B9),
    cardColor: Colors.white,
    errorColor: Color(0xFFFE6767),
    primaryColorLight: Colors.grey[300],
    backgroundColor: Colors.white,
    shadowColor: Color(0x12D8D8D8),
    //fontFamily: 'Helvetica',
    fontFamily: 'Lato',
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor:Colors.transparent,
    dividerColor: Color(0xFFDFE1E2),
    canvasColor: Color(0xFF2C2E31),
    hintColor: Color(0xFF9EA5A3),
    buttonColor: Color(0xFF02B2B9),
    primarySwatch: MaterialColors.colorLight,
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: Color(0xFFFBFBFC),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
      )
    ),
    buttonTheme: ButtonThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 20,),
      buttonColor: Color(0xFF02B2B9),
      splashColor: Color(0xFF028B91),
      disabledColor: Color(0xFFD8D8D8),
      textTheme: ButtonTextTheme.primary,
    ),
    cardTheme: CardTheme(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      elevation: 2,
      shadowColor: Color(0x12D8D8D8),
    ),
    dialogTheme: DialogTheme(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      elevation: 6,
      backgroundColor: Color(0xFFFBFBFC),
    ),
    appBarTheme: AppBarTheme(
      color: Colors.white,
      elevation: 0,
      iconTheme: IconThemeData(
        color: Color(0xFF2C2E31),
        size: 26,
      ),
      titleTextStyle: TextStyle(
        color: Color(0xFF2C2E31),
        fontWeight: FontWeight.w600,
        fontSize: 18,
      ),
    ),
    dividerTheme: DividerThemeData(
      thickness: 0.35,
      color: Color(0xA2D8D8D8),
      space: 2,
    ),
    dialogBackgroundColor: Color(0xFFFBFBFC),
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      bodyText1: TextStyle(color: Color(0xFF2C2E31), fontSize: 14, fontWeight: FontWeight.w300,),
      bodyText2: TextStyle(color: Color(0xFF2C2E31), fontSize: 14, fontWeight: FontWeight.w400,),
      button: TextStyle(color: Color(0xFFFFFFFF), fontSize: 16, fontWeight: FontWeight.w400,),
      subtitle1: TextStyle(color: Color(0xFF02B2B9), fontSize: 14, fontWeight: FontWeight.w300,),
      subtitle2: TextStyle(color: Color(0xFF02B2B9), fontSize: 14, fontWeight: FontWeight.w400,),
      caption: TextStyle(color: Color(0xFF9EA5A3), fontSize: 12, fontWeight: FontWeight.w300,),
      headline1: TextStyle(color: Color(0xFF02B2B9), fontSize: 24, fontWeight: FontWeight.w300,),
      headline2: TextStyle(color: Color(0xFF02B2B9), fontSize: 24, fontWeight: FontWeight.w600,),
      headline3: TextStyle(color: Color(0xFF2C2E31), fontSize: 20, fontWeight: FontWeight.w300,),
      headline4: TextStyle(color: Color(0xFF2C2E31), fontSize: 16, fontWeight: FontWeight.w600,),
      headline5: TextStyle(color: Color(0xFF2C2E31), fontSize: 16, fontWeight: FontWeight.w400,),
      headline6: TextStyle(color: Color(0xFF2C2E31), fontSize: 14, fontWeight: FontWeight.w600,),
    ),
  ),
  AppTheme.Dark: ThemeData(
    brightness: Brightness.dark,
    primaryColor: Color(0xff023355),
    primaryColorDark: Color(0xff023355),
    scaffoldBackgroundColor: Color(0xff022640),
    disabledColor: Color(0xff095589),
    //scaffoldBackgroundColor: Color(0xFFD6F1F2),
    accentColor: Color(0xFFFFCF00),
    cursorColor: Color(0xFF02B2B9),
    cardColor: Color(0xff023355),
    errorColor: Color(0xFFFE6767),
    primaryColorLight: Colors.grey[300],
    backgroundColor: Color(0xff023355),
    shadowColor: Color(0x18030303),
    //fontFamily: 'Helvetica',
    fontFamily: 'Lato',
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor:Colors.transparent,
    dividerColor: Color(0xFFDFE1E2),
    canvasColor: Color(0xFF2C2E31),
    hintColor: Color(0xB8FFFFFF),
    buttonColor: Color(0xFF02B2B9),
    primarySwatch: MaterialColors.colorLight,
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: Color(0xff023355),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
      ),
    ),
    buttonTheme: ButtonThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 20,),
      buttonColor: Color(0xFF02B2B9),
      splashColor: Color(0xFF028B91),
      disabledColor: Color(0xff022640),
    ),
    cardTheme: CardTheme(
      color: Color(0xff023355),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      elevation: 5,
      shadowColor: Color(0x18030303),
    ),
    dialogTheme: DialogTheme(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      elevation: 6,
      backgroundColor: Color(0xff023355),
    ),
    appBarTheme: AppBarTheme(
      color: Color(0xff023355),
      elevation: 0,
      iconTheme: IconThemeData(
        color: Color(0xFFFFFFFF),
        size: 26,
      ),
      titleTextStyle: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w600,
        fontSize: 18,
      ),
    ),
    dividerTheme: DividerThemeData(
      thickness: 0.35,
      space: 2,
      color: Color(0x62ffffff),
    ),
    dialogBackgroundColor: Color(0xFFFBFBFC),
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      bodyText1: TextStyle(color: Color(0xFFFFFFFF), fontSize: 14, fontWeight: FontWeight.w300,),
      bodyText2: TextStyle(color: Color(0xFFFFFFFF), fontSize: 14, fontWeight: FontWeight.w400,),
      button: TextStyle(color: Color(0xFFFFFFFF), fontSize: 16, fontWeight: FontWeight.w400,),
      subtitle1: TextStyle(color: Color(0xFF02B2B9), fontSize: 14, fontWeight: FontWeight.w300,),
      subtitle2: TextStyle(color: Color(0xFF02B2B9), fontSize: 14, fontWeight: FontWeight.w400,),
      caption: TextStyle(color: Color(0xB8FFFFFF), fontSize: 12, fontWeight: FontWeight.w300,),
      headline1: TextStyle(color: Color(0xFF02B2B9), fontSize: 24, fontWeight: FontWeight.w300,),
      headline2: TextStyle(color: Color(0xFF02B2B9), fontSize: 24, fontWeight: FontWeight.w600,),
      headline3: TextStyle(color: Color(0xFFFFFFFF), fontSize: 20, fontWeight: FontWeight.w300,),
      headline4: TextStyle(color: Color(0xFFFFFFFF), fontSize: 16, fontWeight: FontWeight.w600,),
      headline5: TextStyle(color: Color(0xFFFFFFFF), fontSize: 16, fontWeight: FontWeight.w400,),
      headline6: TextStyle(color: Color(0xFFFFFFFF), fontSize: 14, fontWeight: FontWeight.w600,),
    ),
  ),
};