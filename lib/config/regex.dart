import 'package:blog/models/common/regex_config.dart';

abstract class RegexConstant {

  static RegexConfig none = RegexConfig(
      pattern: r'.{0,}',
      errorText: 'No validate'
  );

  static RegexConfig notEmpty = RegexConfig(
    pattern: r'.{1,}',
    errorText: 'Field shouldn\'t be not empty'
  );

  static RegexConfig email = RegexConfig(
    pattern: r'^[\w-\.+]+@([\w-]+\.)+[\w-]{2,4}$',
    errorText: 'Field should contain valid email'
  );

  static RegexConfig password = RegexConfig(
    pattern: r'^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*(),.?":{}|<>])[a-zA-Z\d!@#$%^&*(),.?":{}|<>]{8,50}$',
    errorText: 'Field contains digits, lower letter, upper letter, special character, and [8,50] characters',
  );

  static RegexConfig username = RegexConfig(
    pattern: r'[\w-]{3,25}',
    errorText: 'Field should contain digits, lower letter, upper letter, and [3,25] characters',
  );

}
