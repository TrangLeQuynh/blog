import 'package:blog/models/common/user.dart';
import 'package:blog/service/user-service.dart';
import 'package:flutter/cupertino.dart';

class UserProvider extends ChangeNotifier {
  User _user;
  User get user => _user;

  Future<User> getUser() async {
    _user = await UserService.getUserInfo();
    if (_user == null) {
      return null;
    }
    _user.friends = await UserService.accountFriends(_user.id);
    _user.posts = await UserService.accountPosts(_user.id);
    notifyListeners();
    return _user;
  }

  Future<void> removeUser() async {
    _user = null;
    notifyListeners();
  }
}
