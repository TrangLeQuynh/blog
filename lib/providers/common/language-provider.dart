import 'package:blog/models/common/language-config.dart';
import 'package:blog/service/config-service.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class LanguageProvider extends ChangeNotifier {

  List<LanguageConfig> languageConfigs;

  LanguageProvider() {
    _fetchLocale();
  }

  Locale _appLocale;

  Locale get appLocal => _appLocale;
  String get getFlat => 'assets/images/flags/${_appLocale.languageCode}.svg';


  Future loadLanguageConfig() async {
    var result = await ConfigService.getConfig();
    languageConfigs = result;
    notifyListeners();
  }

  void loadNewLanguageSuccess(localeStr) {
    _appLocale = Locale(localeStr);
    notifyListeners();
  }

  void _fetchLocale() async {
    String code = await SharedPrefsService.getLanguageCode();
    _appLocale = Locale(code);
    notifyListeners();
  }
}
