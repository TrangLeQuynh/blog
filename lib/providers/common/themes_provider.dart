import 'package:blog/utils/shared_prefs_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blog/config/theme.dart';

class ThemesManager extends ChangeNotifier {

  ThemeData _themeData;
  ThemeData get themeData {
    if (_themeData == null) {
      _themeData = appThemeData[AppTheme.Light];
    }
    return _themeData;
  }

  ThemesManager() {
    _loadTheme();
  }

  void changeThemes(AppTheme appTheme) {
    _themeData = appThemeData[appTheme];
    SharedPrefsService.setThemeMode(appTheme);
    notifyListeners();
  }

  void _loadTheme() async {
    _themeData = await SharedPrefsService.getThemeMode();
    notifyListeners();
  }
}
