part of 'chat-room.dart';

ChatRoom _$ChatRoomFromJson(Map<String, dynamic> json) {
  return ChatRoom(
    id: json['id'] as num,
    chatId: json['chatId'] as String,
    senderId: json['senderId'] as num,
    recipient: User.fromJson(json['recipient'] as Map<String, dynamic>),
    status: json['status'] as bool,
  );
}

Map<String, dynamic> _$ChatRoomToJson(ChatRoom instance) =>
    <String, dynamic>{
      'id': instance.id,
      'chatId': instance.chatId,
      'senderId': instance.senderId,
      'recipientId': instance.recipient.id,
      'status' : instance.status
    };
