import 'package:blog/models/common/user.dart';

part 'chat-room.g.dart';

class ChatRoom {
  num id;
  String chatId;
  num senderId;
  User recipient;
  bool status;

  ChatRoom({ this.id, this.chatId, this.senderId, this.recipient, this.status = false, });

  factory ChatRoom.fromJson(Map<String, dynamic> json) => _$ChatRoomFromJson(json);

  Map<String, dynamic> toJson() => _$ChatRoomToJson(this);
}
