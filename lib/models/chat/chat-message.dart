part 'chat-message.g.dart';

class ChatMessage {
  num id;
  String chatId;
  num senderId;
  num recipientId;
  String senderName;
  String content;
  num createAt;

  ChatMessage({
    this.id,
    this.chatId,
    this.senderId,
    this.senderName,
    this.recipientId,
    this.content,
    this.createAt,
  });

  factory ChatMessage.fromJson(Map<String, dynamic> json) => _$ChatMessageFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessageToJson(this);

}
