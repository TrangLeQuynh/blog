part of 'chat-message.dart';

ChatMessage _$ChatMessageFromJson(Map<String, dynamic> json) {
  return ChatMessage(
    id: json['id'] as num,
    chatId: json['chatId'] as String,
    senderId: json['senderId'] as num,
    recipientId: json['recipientId'] as num,
    content: json['content'] as String,
    createAt: json['createAt'] as num
  );
}

Map<String, dynamic> _$ChatMessageToJson(ChatMessage instance) =>
  <String, dynamic>{
    'id': instance.id,
    'chatId': instance.chatId,
    'senderId': instance.senderId,
    'recipientId': instance.recipientId,
    'content': instance.content,
    'createAt': instance.createAt,
    'senderName': instance.senderName,
  };
