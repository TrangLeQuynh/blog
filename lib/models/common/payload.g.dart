part of 'payload.dart';

Payload _$PayloadFromJson(Map<String, dynamic> json) {
  return Payload(
    type: json['type'] as num,
    id: json['id'] as num,
    senderName: json['senderName'] as String,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$PayloadToJson(Payload instance) {
  return <String, dynamic> {
    'type' : instance.type,
    'id' : instance.id,
    'senderName' : instance.senderName,
    'description' : instance.description,
  };
}
