import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {

  num id;
  String username;
  String email;
  String password;
  String address;
  String avatar;
  String cover;
  num friends;
  num posts;
  bool role;
  bool enable;
  // DateTime createAt;
  // DateTime updateAt;

  User({
    this.id,
    this.username,
    this.email,
    this.password,
    this.address,
    this.avatar,
    this.cover,
    this.enable,
    this.role,
    this.friends,
    this.posts,
    // this.createAt,
    // this.updateAt,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() {
    return 'User { username: $username , \n avatar: $avatar }';
  }
}