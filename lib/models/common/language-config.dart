class LanguageConfig {
  String code;

  String name;

  String flag;

  bool status;

  LanguageConfig({ this.code, this.name, this.flag, this.status });

  factory LanguageConfig.fromJson(Map<String, dynamic> json) {
    return LanguageConfig(
      code: json['code'],
      name: json['name'],
      flag: json['flag'],
      status: json['status'],
    );
  }
}
