import 'package:flutter/cupertino.dart';
part 'payload.g.dart';

class Payload {

  num type;
  String senderName;
  num id;
  String description;

  Payload({
    this.id,
    @required this.senderName,
    @required this.type,
    this.description,
  });

  factory Payload.fromJson(Map<String, dynamic> json) => _$PayloadFromJson(json);

  Map<String, dynamic> toJson() => _$PayloadToJson(this);
}
