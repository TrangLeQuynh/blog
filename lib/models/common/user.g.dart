part of 'user.dart';

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as num,
    username: json['username'] as String,
    email: json['email'] as String,
    password: json['password'] as String,
    address: json['address'] as String,
    avatar: json['avatar'] as String,
    cover: json['cover'] as String,
    enable: json['enable'] as bool,
    role: json['role'] as bool,
    // createAt: json['createAt'] as DateTime,
    // updateAt: json['updateAt'] as DateTime,
  );
}

Map<String, dynamic> _$UserToJson(User instance) {
  return <String, dynamic>{
    'id': instance.id,
    'username': instance.username,
    'email': instance.email,
    'password': instance.password,
    'address': instance.address,
    'avatar': instance.avatar,
    'cover' : instance.cover,
    'enable': instance.enable,
    'role': instance.role,
    // 'createAt': instance.createAt,
    // 'updateAt': instance.updateAt,
  };
}