import 'package:json_annotation/json_annotation.dart';

part 'verification.g.dart';

@JsonSerializable()
class Verification {

  num id;

  String verifyCode;

  DateTime expire;

  Verification(this.id, this.verifyCode, this.expire);

  factory Verification.fromJson(Map<String, dynamic> json) => _$VerificationFromJson(json);

  Map<String, dynamic> toJson() => _$VerificationToJson(this);

  @override
  String toString() {
    return 'Token { accessToken: $id , \n refreshToken: $verifyCode }';
  }
}