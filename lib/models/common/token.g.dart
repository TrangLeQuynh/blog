part of 'token.dart';

Token _$TokenFromJson(Map<String, dynamic> json) {
  return Token(
    json['accessToken'] as String,
    json['refreshToken'] as String,
  );
}

Map<String, dynamic> _$TokenToJson(Token instance) {
  return <String, dynamic>{
    'accessToken': instance.accessToken,
    'refreshToken': instance.refreshToken,
  };
}