part of 'verification.dart';

Verification _$VerificationFromJson(Map<String, dynamic> json) {
  return Verification(
    json['id'] as num,
    json['verify_code'] as String,
    json['expire'] as DateTime
  );
}

Map<String, dynamic> _$VerificationToJson(Verification instance) {
  return <String, dynamic> {
    'id' : instance.id,
    'verify_code' : instance.verifyCode,
    'expire' : instance.expire
  };
}