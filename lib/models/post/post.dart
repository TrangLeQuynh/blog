import 'dart:core';

import 'package:blog/models/common/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post.g.dart';

@JsonSerializable()
class Post {

  num id;
  User user;
  String content;
  String hashTag;
  List<String> images;
  double latitude;
  double longitude;
  num createAt;
  num updateAt;

  Post({
    this.id,
    this.user,
    this.content,
    this.hashTag,
    this.images,
    this.longitude,
    this.latitude,
    this.createAt,
    this.updateAt
  });

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Map<String, dynamic> toJson() => _$PostToJson(this);

}