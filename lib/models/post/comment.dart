
import 'package:blog/models/common/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'comment.g.dart';

@JsonSerializable()
class Comment {
  num id;
  num postId;
  User user;
  String content;
  num createAt;
  num updateAt;

  Comment({
    this.id,
    this.postId,
    this.user,
    this.content,
    this.createAt,
    this.updateAt
  });

  factory Comment.fromJson(Map<String, dynamic> json) => _$CommentFromJson(json);

  Map<String, dynamic> toJson() => _$CommentToJson(this);
}