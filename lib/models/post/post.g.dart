part of 'post.dart';


Post _$PostFromJson(Map<String, dynamic> json) {
  return Post(
    id: json['id'] as num,
    user: User.fromJson(json['user'] as Map<String, dynamic>),
    content: json['content'] as String,
    hashTag: json['hashTag'] as String,
    images: (json['images'] as List)?.map((e) => e as String)?.toList(),
    latitude: json['latitude'] as double,
    longitude: json['longitude'] as double,
    createAt: json['createAt'] as num,
    updateAt: json['updateAt'] as num,
  );
}

Map<String, dynamic> _$PostToJson(Post instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user': instance.user.toJson(),
      'content': instance.content,
      'hash_tag': instance.hashTag,
      'images': instance.images,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'createAt': instance.createAt,
      'updateAt': instance.updateAt,
    };
