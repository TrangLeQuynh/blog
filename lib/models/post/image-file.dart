import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ImageFile {
  String link;
  bool type;

  ImageFile({
    this.link,
    this.type = true,
  });

  String getType() => type ? "File" : "URL";

}