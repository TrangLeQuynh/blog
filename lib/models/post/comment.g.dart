part of 'comment.dart';

Comment _$CommentFromJson(Map<String, dynamic> json) {
  return Comment(
    id: json['id'] as num,
    postId: json['postId'] as num,
    user: User.fromJson(json['user'] as Map<String, dynamic>),
    content: json['content'] as String,
    createAt: json['createAt'] as num,
    updateAt: json['updateAt'] as num,
  );
}

Map<String, dynamic> _$CommentToJson(Comment instance) =>
  <String, dynamic>{
    'id': instance.id ,
    'postId': instance.postId,
    'user': instance?.user?.toJson(),
    'content': instance.content,
    'createAt': instance.createAt,
    'updateAt': instance.updateAt,
  };