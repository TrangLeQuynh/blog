part of 'friend.dart';

Friend _$FriendFromJson(Map<String, dynamic> json) {
  return Friend(
    user: User.fromJson(json['id']['user'] as Map<String, dynamic>),
    friend: User.fromJson(json['id']['friend'] as Map<String, dynamic>),
    status: json['status'] as String,
    createAt: json['createAt'] as num,
  );
}

Map<String, dynamic> _$FriendToJson(Friend instance) =>
    <String, dynamic>{
      'id' : {
        'user': instance.user.toJson(),
        'friend': instance.friend.toJson(),
      },
      'status': instance.status,
      'createAt': instance.createAt,
    };