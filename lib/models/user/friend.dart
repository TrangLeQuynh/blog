import 'package:blog/models/common/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'friend.g.dart';

@JsonSerializable()
class Friend {

  User user;
  User friend;
  num createAt;
  String status;

  Friend({ this.user, this.friend, this.createAt, this.status, });

  factory Friend.fromJson(Map<String, dynamic> json) => _$FriendFromJson(json);

  Map<String, dynamic> toJson() => _$FriendToJson(this);

}