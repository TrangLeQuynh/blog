import 'package:blog/plugin/locator.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:blog/providers/common/language-provider.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/router/router.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/service/notification-service.dart';
import 'package:flutter/material.dart';
import 'package:blog/providers/common/themes_provider.dart';
import 'package:blog/providers/common/connectivity-provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'i18n/localization-delegate.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  //initialize notification
  await NotificationService().init();
  //lock the device orientation
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ThemesManager()),
        ChangeNotifierProvider(create: (context) => LanguageProvider()),
        ChangeNotifierProvider(create: (context) => ConnectivityProvider()),
        ChangeNotifierProvider(create: (context) => UserProvider()),
      ],
      child: MyApp(),
    )
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Consumer2<LanguageProvider, ThemesManager>(
        builder: (context, language, themes, child) {
          print('language code : ${language?.appLocal?.languageCode}');
            return MaterialApp(
              title: 'Blog',
              theme: themes.themeData,
              initialRoute: RoutingNameConstant.splashScreen,
              navigatorKey: locator<NavigatorService>().navigatorKey,
              routes: RouterConstant.routes,
              debugShowCheckedModeBanner: false,
              localizationsDelegates: [const I18nLocalizationsDelegate()],
            );
        }
      ),
      onTap: () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        FocusScope.of(context).requestFocus(new FocusNode());
      },
    );
  }
}
