import 'dart:async';  

import 'package:flutter/material.dart';
import 'package:blog/i18n/localization.dart';

class I18nLocalizationsDelegate extends LocalizationsDelegate<I18nLocalizations> {
  const I18nLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['vi', 'en'].contains(locale.languageCode);

  @override
  Future<I18nLocalizations> load(Locale locale) async {
    I18nLocalizations localizations = new I18nLocalizations(locale);
    return localizations;
  }

  @override
  bool shouldReload(I18nLocalizationsDelegate old) => false;
}
