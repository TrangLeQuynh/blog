import 'dart:async';
import 'package:blog/service/config-service.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:flutter/material.dart';

class I18nLocalizations {
  I18nLocalizations(this.locale);

  Locale locale;

  static I18nLocalizations of(BuildContext context) {
    return Localizations.of<I18nLocalizations>(context, I18nLocalizations);
  }

  Map<String, String> _sentences;

  Future<bool> loadByLocale(Locale locale) async {
    String code = await SharedPrefsService.getLanguageCode();
    Map<String, dynamic> _result = await ConfigService.getLanguages(locale.languageCode);
    this.locale = locale;
    this._sentences = new Map();
    _result.forEach((String key, dynamic value) {
      this._sentences[key] = value.toString();
    });
    SharedPrefsService.setLanguages(this._sentences);
    return true;
  }

  String trans(String key) {
    if (this._sentences[key] != null) return this._sentences[key];
    return key;
  }
}
