import 'package:blog/config/constants.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/plugin/locator.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

enum DialogType { NONE, YES_NO, CONFIRM_CANCEL, CANCEL }

final currentContext = locator<NavigatorService>().navigatorKey.currentState.overlay.context;

abstract class DialogBuilder {

  static final _theme = Theme.of(currentContext);


  static Future<bool> buildNotificationDialog({
    String urlImage,
    DialogType dialogType = DialogType.NONE,
    VoidCallback function,
    bool barrierDismissible = false,
    Widget content,
    String message,
    Color color,
  }) async {

    Widget _buildAction() {
      switch (dialogType) {
        case DialogType.NONE:
          return SizedBox(height: 10,);
        case DialogType.CANCEL:
          return buildButton(
            title: I18nLocalizations.of(currentContext).trans('BTN_CANCEL'),
            textColor: Theme.of(currentContext).errorColor,
            onPress: () {
              Navigator.of(currentContext).pop(false);
            },
          );
        case DialogType.YES_NO:
          return _buildDoubleActions(
            leftLabel: I18nLocalizations.of(currentContext).trans('BTN_YES'),
            rightLabel: I18nLocalizations.of(currentContext).trans('BTN_NO'),
            function: function,
          );
        case DialogType.CONFIRM_CANCEL:
          return _buildDoubleActions(
            leftLabel: I18nLocalizations.of(currentContext).trans('BTN_CONFIRM'),
            rightLabel: I18nLocalizations.of(currentContext).trans('BTN_CANCEL'),
            function: function,
          );
        default:
          return SizedBox(height: 10,);
      }
    }

    Widget _content = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(height: 35,),
        SizedBox(
          height: 46,
          width: 46,
          child: SvgPicture.asset(
            urlImage ?? 'assets/images/system/ic_communication.svg',
            fit: BoxFit.contain,
            color: color,
          ),
        ),
        const SizedBox(height: 20,),
        Padding(
          child: Text(
            message ?? '',
            style: Theme.of(currentContext).textTheme.headline5,
            textAlign: TextAlign.center,
          ),
          padding: const EdgeInsets.symmetric(horizontal: 35,),
        ),
        content ?? const SizedBox(),
        const SizedBox(height: 25,),
        DialogType.NONE != dialogType ? const Divider() : const SizedBox(),
        _buildAction(),
      ],
    );

    AlertDialog alertDialog = new AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      content: _content,
    );

    bool result = await showDialog(
      context: currentContext,
      barrierDismissible: barrierDismissible,
      builder: (_) {
        if (dialogType == DialogType.NONE && !barrierDismissible) {
          Future.delayed(Duration(milliseconds: 3500)).then((value) {
            Navigator.of(currentContext).pop(false);
          });
        }
        return WillPopScope(
          child: alertDialog,
          onWillPop: () async => barrierDismissible,
        );
      }
    );
    return result;
  }

  static void buildLoadingDialog() {
    Constants.isLoadingDialog = true;
    showDialog(
      context: currentContext,
      barrierDismissible: true,
      barrierColor: Theme.of(currentContext).disabledColor.withOpacity(0.3),
      builder: (_) {
        return WillPopScope(
          child: Center(
            child: SizedBox(
              height: 32,
              width: 32,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                backgroundColor: Colors.white.withOpacity(0.03),
              ),
            ),
          ),
          onWillPop: () async => false
        );
      }
    );
  }

}

Widget buildButton({
  VoidCallback onPress,
  @required String title,
  Color textColor,
}) {
  return FlatButton(
    onPressed: onPress,
    splashColor: Theme.of(currentContext).hintColor,
    padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10,),
    child: Text(
      title,
      style: Theme.of(currentContext).textTheme.headline4.copyWith(
        color: textColor ?? Theme.of(currentContext).primaryColor,
      ),
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(0)),
    ),
  );
}

Widget _buildDoubleActions({ @required String leftLabel, @required String rightLabel, VoidCallback function, }) {
  return  Row(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(
        child: buildButton(
          title: leftLabel,
          textColor: Theme.of(currentContext).cursorColor,
          onPress: function ?? () {
            Navigator.of(currentContext).pop(true);
          },
        ),
      ),
      const SizedBox(
        height: 46,
        child: VerticalDivider(),
      ),
      Expanded(
        child: buildButton(
          title: rightLabel,
          textColor: Theme.of(currentContext).errorColor,
          onPress: () {
            Navigator.of(currentContext).pop(false);
          },
        ),
      ),
    ],
  );
}