import 'package:blog/i18n/localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

abstract class SnackBarBuilder {

  static void showSnackBar(BuildContext context, { String message = '', bool status = false, }) {
    Fluttertoast.showToast(
      msg: I18nLocalizations.of(context).trans(message),
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 3,
      backgroundColor: status ? Theme.of(context).buttonColor : Theme.of(context).errorColor,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }
}