import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LengthLimitation extends TextInputFormatter {
  final int maxLength;
  LengthLimitation({ @required this.maxLength });

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > maxLength)
      return oldValue;
    return newValue;
  }

}


//user for number
class InputFormatterNumber extends TextInputFormatter {

  RegExp regExp;
  InputFormatterNumber(this.regExp);

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text == '.') {
      return TextEditingValue(
        text: '0.',
        selection: TextSelection.collapsed(offset: 2),
      );
    }
    RegExp firstZero = new RegExp(r'^0{2,}$');
    if (firstZero.hasMatch(newValue.text)) {
      return TextEditingValue(
        text: '0',
        selection: TextSelection.collapsed(offset: 1),
      );
    }
    if (regExp.hasMatch(newValue.text))
      return newValue;
    return oldValue;
  }

}