import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class AppBarBuilder {

  static AppBar primaryAppbar(BuildContext context, { String title }) {
    return AppBar(
      toolbarHeight: 50,
      automaticallyImplyLeading: false,
      leadingWidth: 100,
      centerTitle: true,
      title: Text(
        title ?? '',
        style: Theme.of(context).textTheme.headline4,
      ),
      leading: Container(
        child: Image.asset(
          'assets/images/logo/logo_gradient.png',
          fit: BoxFit.contain,
          alignment: Alignment.centerLeft,
        ),
        padding: const EdgeInsets.only(top: 8, bottom: 8, left: 20, right: 0)
      ),
    );
  }

  static AppBar baseAppBar(BuildContext context, {
    String title,
    double elevation = 0,
    List<Widget> actions,
    Color backgroundColor,
    IconThemeData iconThemeData,
    bool automaticallyImplyLeading = true,
  }) {
    return AppBar(
      iconTheme: iconThemeData,
      backgroundColor: backgroundColor,
      automaticallyImplyLeading: automaticallyImplyLeading,
      toolbarHeight: 50,
      elevation: elevation,
      centerTitle: true,
      title: Text(
        title ?? '',
        style: Theme.of(context).textTheme.headline4,
      ),
      actions: actions,
    );
  }


}
