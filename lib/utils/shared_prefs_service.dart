import 'dart:convert';

import 'package:blog/config/theme.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsService {

  static Future<bool> setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = await prefs.setString('token', token);
    return result;
  }

  static Future<String> getToken() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String jsonToken = prefs.getString('token') ?? '';
      return jsonToken;
    } catch (e) {
      return '';
    }
  }

  static removeToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
  }

  static void setLanguageCode (String code) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('language', code);
  }

  static Future<String> getLanguageCode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String code = prefs.getString('language');
    if (code == null) return 'vi';
    else return code;
  }

  static Future<Map> getLanguages() async {
    var prefs = await SharedPreferences.getInstance();
    String languages = prefs.getString('languages');
    if(languages == null)
      return null;
    Map<String, String> map = Map.castFrom(json.decode(languages));
    return map;
  }

  static Future setLanguages(Map languages) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('languages', jsonEncode(languages));
  }

  static void setThemeMode (AppTheme theme) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt('themeMode', AppTheme.values.indexOf(theme));
  }

  static Future<ThemeData> getThemeMode() async {
    var prefs = await SharedPreferences.getInstance();
    int preferredTheme = prefs.getInt('themeMode') ?? 0;
    ThemeData _themeData = appThemeData[AppTheme.values[preferredTheme]];
    return _themeData;
  }
}
