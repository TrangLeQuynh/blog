import 'dart:convert';
import 'dart:io';
import 'package:blog/config/constants.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:path/path.dart' as path;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

abstract class UtilServices {

  static bool isDartTheme(BuildContext context) {
    if (Theme.of(context).brightness == Brightness.light) {
      return false;
    }
    return true;
  }

  static void closeLoadingDialog(BuildContext context) {
    if (!Constants.isLoadingDialog)
      return;
    Constants.isLoadingDialog = false;
    Navigator.of(context).pop();
  }

  static Future<String> convertToBase64(File file) async {
    final bytes = await file.readAsBytes();
    String img64 = base64Encode(bytes);
    String type = path.extension(file.path).split('.')[1];
    //data:image/$type;base64,$img64
    return 'data:image/$type;base64,$img64';
  }

  static Future<void> logout(BuildContext context) async {
    SharedPrefsService.removeToken();
    Provider.of<UserProvider>(context, listen: false).removeUser();
  }

  static String textCapitalize(String str) {
    if (str == null) return '';
    if (str.length <= 1) return str.toUpperCase();
    return '${str[0].toUpperCase()}${str.substring(1)}';
  }
}
