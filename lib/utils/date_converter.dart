import 'package:intl/intl.dart';

class DateConverter {
  static String convertDate(num timestamp, { String formatDate = 'dd-MM-yyyy hh:mm a', }) {
    try {
      if(timestamp == null || timestamp == 0)
        return '';
      var date = new DateTime.fromMillisecondsSinceEpoch(timestamp);
      return DateFormat(formatDate).format(date);
    } catch (e) {
      return '';
    }
  }
}
