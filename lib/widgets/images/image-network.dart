import 'package:blog/config/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget imageNetwork({ String url, BoxFit boxFit }) {
  return CachedNetworkImage(
    imageUrl: '${Constants.URL_IMAGE}$url',
    fit: boxFit ?? BoxFit.contain,
    alignment: Alignment.center,
    errorWidget: (context, error, stackTrace) {
      return Container(
        padding: const EdgeInsets.all(10.0),
        color: Theme.of(context).scaffoldBackgroundColor,
        alignment: Alignment.center,
        child: SizedBox(
          height: 55,
          width: 55,
          child: SvgPicture.asset(
            'assets/images/icons/ic_image.svg',
            fit: BoxFit.contain,
            alignment: Alignment.center,
            color: Theme.of(context).disabledColor.withOpacity(0.5),
          ),
        ),
      );
    },
  );
  // return Image.network(
  //   '${Constants.URL_IMAGE}$url',
  //   fit: boxFit ?? BoxFit.contain,
  //   alignment: Alignment.center,
  //   loadingBuilder: (context, child, loadingProgress) {
  //     if (loadingProgress == null) return child;
  //     return Container(
  //       alignment: Alignment.center,
  //       color: Theme.of(context).scaffoldBackgroundColor,
  //       child: SizedBox(
  //         width: 32,
  //         height: 32,
  //         child: CircularProgressIndicator(strokeWidth: 2,),
  //     ),);
  //   },
  //   errorBuilder: (context, error, stackTrace) {
  //     return Container(
  //       padding: const EdgeInsets.all(10.0),
  //       color: Theme.of(context).scaffoldBackgroundColor,
  //       alignment: Alignment.center,
  //       child: SizedBox(
  //         height: 55,
  //         width: 55,
  //         child: SvgPicture.asset(
  //           'assets/images/icons/ic_image.svg',
  //           fit: BoxFit.contain,
  //           alignment: Alignment.center,
  //           color: Theme.of(context).disabledColor.withOpacity(0.5),
  //         ),
  //       ),
  //     );
  //   },
  // );
}