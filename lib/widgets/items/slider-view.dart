import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SliderView extends StatefulWidget {

  bool autoPlay;
  Color colorIndicator;
  List<Widget> items;

  SliderView({
    this.autoPlay,
    this.colorIndicator,
    @required this.items,
  });

  @override
  _SliderViewState createState() => _SliderViewState();
}

class _SliderViewState extends State<SliderView> {
  int _current = 0;
  CarouselController _carouselController = new CarouselController();
  PageController _pageController = new PageController(initialPage: 0, );

  @override
  void dispose() {
    _carouselController = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: CarouselSlider(
            options: CarouselOptions(
              reverse: false,
              height: MediaQuery.of(context).size.width,
              viewportFraction: 0.9,
              autoPlay: widget.autoPlay ?? true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 1200),
              autoPlayCurve: Curves.fastOutSlowIn,
              pauseAutoPlayOnTouch: true,
              enlargeCenterPage: true,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              },
              aspectRatio: 1
            ),
            items: widget.items,
            carouselController: _carouselController,
          ),
        ),
        const SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _indicatorItems(),
        ),
      ],
    );
  }

  List<Widget> _indicatorItems () => List.generate (widget.items.length, (index) {
    return Container(
      width: 10.0,
      height: 10.0,
      margin: EdgeInsets.symmetric (vertical: 15.0, horizontal: 2.0),
      decoration: _current == index
      ? BoxDecoration(
        shape: BoxShape.circle,
        color: widget.colorIndicator ?? Theme.of(context).accentColor,
      )
      : BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(width: 1, color: Color(0xFFCBCBCB))
      )
    );
  });

}
