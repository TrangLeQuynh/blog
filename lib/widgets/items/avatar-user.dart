import 'package:blog/config/constants.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/widgets/images/image-network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AvatarWidget extends StatelessWidget {

  final double size;
  final String url;

  AvatarWidget({ this.url, this.size = 36, });

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Container(
        height: size,
        width: size,
        color: Theme.of(context).scaffoldBackgroundColor,
        child: url != null && url != ''
          ? imageNetwork(
            url: '$url',
            boxFit: BoxFit.cover,
          )
          : Image.asset(
            'assets/images/system/male.png',
          ),
      ),
    );
  }

}