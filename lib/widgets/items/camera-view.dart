import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CameraView extends StatefulWidget {

  @override
  CameraState createState() => CameraState();

}

class CameraState extends State<CameraView> {

  List<CameraDescription> cameras;
  //index 0: back camera - index 1: front camera
  int cameraIndex = 0;
  CameraController _controller;
  String path;
  bool isCamera = true;

  Future _initCameraController(CameraDescription cameraDescription) async {

    if (_controller != null)
      await _controller?.dispose();

    _controller = CameraController(cameraDescription, ResolutionPreset.high);

    try {
      await _controller.initialize();
    } on CameraException catch (e) {
      print(e.description);
      Navigator.of(context).pop();
    }

    if (this.mounted) {
      setState(() {

      });
    }

  }

  @override
  void initState() {
    super.initState();

    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.isNotEmpty) {
        _initCameraController(
          cameras[cameraIndex]
        );
      } else {
        Navigator.of(context).pop();
      }
    }).catchError((onError) {
      print('ON ERROR: ${onError.toString()}');
      Navigator.of(context).pop();
    });

  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: [
          SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20,),
                    child: Icon(
                      Icons.arrow_back,
                      size: 28,
                      color: Colors.grey,
                    ),
                  ),
                ),
                _switchCameraType(),
              ],
            ),
          ),
          Expanded(
            child: _cameraPreview(),
          ),
          Align(
            alignment: Alignment.center,
            child: isCamera
            ? GestureDetector(
              onTap: _takePhoto,
              child: Container(
                height: 75,
                child: Icon(Icons.camera, size: 60, color: Colors.grey,),
                padding: const EdgeInsets.all(15),
              ),
            )
            : Container(
              height: 75,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _buttonAction(
                    iconData: Icons.done,
                    color: Theme.of(context).cursorColor,
                    function: () {
                      Navigator.of(context).pop(path);
                    }
                  ),
                  _buttonAction(
                    iconData: Icons.cancel,
                    color: Theme.of(context).errorColor,
                    function: () {
                      setState(() {
                        path = null;
                        isCamera = true;
                      });
                    }
                  ),
                ],
              ),
            )
          ),
        ],
      )
    );
  }

  Widget _buttonAction({ @required IconData iconData, @required Color color, GestureTapCallback function }) {
    return GestureDetector(
      onTap: function,
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Icon(
          iconData,
          color: color,
          size: 46,
        ),
      ),
    );
  }

  Widget _switchCameraType() {
    return Align(
      alignment: Alignment.topRight,
      child: FlatButton(
        onPressed: () {
          if (cameras.length >= 2) {
            setState(() {
              cameraIndex = cameraIndex == 1 ? 0 : 1;
            });
            _initCameraController(cameras[cameraIndex]);
          }
        },
        child: Icon(
          Icons.camera_alt_outlined,
          size: 32,
          color: Colors.grey,
        ),
      ),
    );
  }

  Widget _cameraPreview() {
    if (_controller == null || !_controller.value.isInitialized) {
      return SizedBox();
    }
    if (!isCamera) {
      return Image.file(
        File(path),
      );
    }
    return CameraPreview(
      _controller,
    );
  }

  Future<XFile> _takePhoto() async {
    if (_controller == null || !_controller.value.isInitialized) {
      print('Error: select a camera first.');
      return null;
    }

    if (_controller.value.isTakingPicture) {
      return null;
    }

    try {
      XFile file = await _controller.takePicture();
      setState(() {
        isCamera = false;
        path = file.path;
      });
      return file;
    } on CameraException catch (e) {
      print(e.description);
      return null;
    }
  }

}