import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

void uploadImage (BuildContext currentContext, { VoidCallback onTapCamera, VoidCallback onTapImage, }) {
  showModalBottomSheet(
    context: currentContext,
    builder: (context) {
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 10,),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(3),
                child: Divider(height: 3, thickness: 3, color: Theme.of(currentContext).hintColor,),
              ),
            ),
            buildItem(
              context,
              title: 'Gallery',
              icon: SvgPicture.asset(
                'assets/images/system/ic_image.svg',
                fit: BoxFit.contain,
              ),
              function: onTapImage,
            ),
            Divider(height: 2,),
            buildItem(
              context,
              title: 'Camera',
              icon: SvgPicture.asset(
                'assets/images/system/ic_camera.svg',
                fit: BoxFit.contain,
              ),
              function: onTapCamera,
            ),
            Divider(height: 2,),
            buildItem(
              context,
              title: 'Cancel',
              icon: Center(
                child: SvgPicture.asset(
                  'assets/images/system/ic_cancel_line.svg',
                  height: 24,
                  width: 24,
                ),
              ),
              function: () {
                Navigator.of(currentContext).pop();
              },
            ),
          ],
        ),
      );
    }
  );
}

Widget buildItem(BuildContext context, { String title, VoidCallback function, @required Widget icon }) {
  return GestureDetector(
    child: Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      color: Colors.transparent,
      child: Row(
        children: [
          const SizedBox(width: 10,),
          SizedBox(
            height: 32,
            width: 32,
            child: icon,
          ),
          const SizedBox(width: 20,),
          Text(
            title ?? '',
            style: Theme.of(context).textTheme.headline5,
          ),
          const SizedBox(width: 10,),
        ],
      ),
    ),
    onTap: function,
  );
}

