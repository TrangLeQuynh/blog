import 'package:blog/config/theme.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/providers/common/themes_provider.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/screens/setting/language-setting.dart';
import 'package:blog/screens/setting/password_edition.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class DrawerView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Theme.of(context).backgroundColor,
        child: Column(
          children: [
            _buildHeader(context),
            Expanded(
              child: SingleChildScrollView(
                child: _buildContent(context),
              ),
            ),
            // _buildItem(
            //   context,
            //   image: '/icons/ic_add_friend.svg',
            //   title: 'Friends',
            //   function: () {
            //     Navigator.of(context).pop();
            //     Navigator.of(context).push(
            //         MaterialPageRoute(builder: (_) => FriendListScreen())
            //     );
            //   },
            // ),
            // Divider(),
            // _buildItem(
            //   context,
            //   image: '/icons/ic_group.svg',
            //   title: 'Groups',
            //   function: () {},
            // ),
            Divider(),
            _buildItem(
              context,
              image: '/icons/ic_language.svg',
              title: I18nLocalizations.of(context).trans('BTN_LANGUAGE'),
              function: () {
                print('Show bottom sheet');
                Navigator.of(context).pop();
                showModalBottomSheet(
                  context: context,
                  builder: (c) {
                    return LanguageSettingView();
                  },
                );
              },
            ),
            Divider(),
            _buildItem(
              context,
              image: '/icons/ic_protect_password.svg',
              title: I18nLocalizations.of(context).trans('BTN_CHANGE_PASSWORD'),
              function: () {
                print('change pass');
                Navigator.of(context).pop();
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => PasswordEdition())
                );
              },
            ),
            Divider(),
            _buildItem(
              context,
              image: '/icons/ic_logout.svg',
              title: I18nLocalizations.of(context).trans('BTN_LOGOUT'),
              function: () {
                UtilServices.logout(context);
                Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.loginScreen, (route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [

      ],
    );
  }

  Widget _buildHeader(BuildContext context) {
    double size = MediaQuery.of(context).size.width / 4;
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.fromLTRB(35, 50, 35, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                width: size,
                decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).backgroundColor, width: 3),
                  shape: BoxShape.circle,
                ),
                child: Consumer<UserProvider>(
                  builder: (context, model, child) {
                    return AvatarWidget(
                      url: model?.user?.avatar,
                      size: size - 6,
                    );
                  },
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Switch(
                      value: UtilServices.isDartTheme(context),
                      onChanged: (value) {
                        AppTheme _theme = value ? AppTheme.Dark : AppTheme.Light;
                        Provider.of<ThemesManager>(context, listen: false).changeThemes(_theme);
                      },
                      inactiveThumbColor: Theme.of(context).backgroundColor,
                      inactiveThumbImage: new AssetImage(
                        'assets/images/icons/ic_sun.png',
                      ),
                      inactiveTrackColor: Theme.of(context).disabledColor,
                      activeColor: Theme.of(context).backgroundColor,
                      activeTrackColor: Colors.black45,
                      activeThumbImage: new AssetImage(
                        'assets/images/icons/ic_moon.png',
                      ),

                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10,),
                      child: Text(
                        Provider.of<UserProvider>(context, listen: true)?.user?.username ?? '',
                        style: Theme.of(context).textTheme.headline4.copyWith(fontSize: 18,),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                    const SizedBox(height: 5,),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10,),

        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, { @required String image, @required String title, Color color, VoidCallback function,}) {
    return GestureDetector(
      onTap: function,
      child: Container(
        color: Colors.transparent,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 35),
        child: Row(
          children: [
            Expanded(
              child: SizedBox(
                height: 28,
                width: double.infinity,
                child: SvgPicture.asset(
                  'assets/images$image',
                  color: color,
                  fit: BoxFit.contain,
                  alignment: Alignment.centerLeft,
                ),
              ),
              flex: 1
            ),
            Expanded(
              flex: 3,
              child: Text(
                title,
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
          ],
        ),
      ),
    );
  }
}