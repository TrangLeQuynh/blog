import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadMoreDelegate extends StatelessWidget {

  bool status;
  LoadMoreDelegate({ @required this.status,});

  @override
  Widget build(BuildContext context) {
    if (status) {
      return Center(
        child: Padding(
          child: SizedBox(
            height: 20,
            width: 20,
            child: CircularProgressIndicator(strokeWidth: 2),
          ),
          padding: const EdgeInsets.all(15),
        ),
      );
    }
    return SizedBox();
  }
}
