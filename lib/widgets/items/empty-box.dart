import 'package:blog/i18n/localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class EmptyBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).disabledColor.withOpacity(0.5);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 120,),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 68,
            width: double.infinity,
            child: SvgPicture.asset(
              'assets/images/icons/ic_no_data.svg',
              fit: BoxFit.contain,
              color: color,
            ),
          ),
          const SizedBox(height: 10,),
          Text(
            I18nLocalizations.of(context).trans('MSG_NO_DATA'),
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ],
      )
    );
  }
}
