// import 'dart:async';
// import 'package:blog/utils/appbar_builder.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';
//
// class GoogleMapsView extends StatefulWidget {
//
//   LocationData locationData;
//
//   GoogleMapsView({required this.locationData});
//
//   @override
//   GoogleMapsState createState() => GoogleMapsState();
//
// }
//
// class GoogleMapsState extends State<GoogleMapsView> {
//
//   Completer<GoogleMapController> _mapController = Completer();
//
//   Set<Marker> _markers = {};
//
//   Future<void> _onMapCreated(GoogleMapController controller) async {
//     late Marker _marker = Marker(
//       markerId: MarkerId('current_id'),
//       position: LatLng(
//         widget.locationData.latitude!,
//         widget.locationData.longitude!,
//       ),
//     );
//
//     setState(() {
//       _markers.clear();
//       _markers.add(_marker);
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBarBuilder.baseAppBar(context, title: 'Google Map'),
//       body: GoogleMap(
//         mapType: MapType.normal,
//         initialCameraPosition: CameraPosition(
//           target: LatLng(
//             widget.locationData.latitude!,
//             widget.locationData.longitude!,
//           ),
//           zoom: 19.8,
//         ),
//         myLocationEnabled: true,
//         onMapCreated: _onMapCreated,
//         // markers: _markers,
//       ),
//     );
//   }
//
// }