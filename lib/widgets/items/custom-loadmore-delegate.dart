import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loadmore/loadmore.dart';

class CustomLoadMoreDelegate extends LoadMoreDelegate {
  BuildContext context;

  CustomLoadMoreDelegate({this.context});

  @override
  Widget buildChild(
    LoadMoreStatus status,
    {LoadMoreTextBuilder builder = DefaultLoadMoreTextBuilder.chinese}) {
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 26,
            height: 26,
            child: CircularProgressIndicator(
              backgroundColor: Theme.of(context).accentColor,
              strokeWidth: 2.3,
            ),
          ),
        ],
      ),
    );
  }
}
