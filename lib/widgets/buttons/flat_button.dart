import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget flatButton(BuildContext context, {
  String title = '',
  String image,
  Color textColor,
  @required Color borderColor,
  Color fillColor,
  VoidCallback onPress}) {
  return FlatButton(
    onPressed: onPress,
    splashColor: borderColor.withOpacity(0.1),
    color: fillColor ?? Colors.transparent,
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15,),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        image == null
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(
                right: 8,
              ),
              child: SvgPicture.asset(
                image,
                fit: BoxFit.contain,
                height: 20,
              ),
            ),
        Text(
          title,
          style: Theme.of(context).textTheme.headline5.copyWith(
            color: Theme.of(context).errorColor,
            fontStyle: FontStyle.italic,
          ),
        ),
      ],
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(25),
      side: BorderSide(
        width: 0.8,
        color: borderColor,
        style: BorderStyle.solid,
      ),
    ),
  );
}
