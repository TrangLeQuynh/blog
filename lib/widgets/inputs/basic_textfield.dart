import 'package:blog/models/common/regex_config.dart';
import 'package:blog/utils/input_formatters.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum BorderType { UNDERLINE, OUTLINE, NONE,}

class BasicTextField extends StatelessWidget {

  final TextEditingController controller;
  final TextEditingController confirmController;
  final bool autoValidate;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final bool enabled;
  final int maxLength;
  final int minLines;
  final int maxLines;
  final String textInputFormatter;
  final String hintText;
  final RegexConfig regexConfig;
  final bool isPassword;
  final BorderType borderType;
  final TextInputAction textInputAction;
  final TextInputType keyboardType;

  BasicTextField({
    this.controller,
    this.confirmController,
    this.autoValidate = false,
    this.prefixIcon,
    this.suffixIcon,
    this.enabled = true,
    this.maxLength = 256,
    this.hintText,
    this.maxLines = 1,
    this.minLines = 1,
    this.textInputFormatter = r'.',
    @required this.regexConfig,
    this.isPassword = false,
    this.borderType = BorderType.UNDERLINE,
    this.textInputAction = TextInputAction.done,
    this.keyboardType,
  });
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      textInputAction: textInputAction,
      keyboardType: keyboardType,
      obscureText: this.isPassword,
      autovalidate: this.autoValidate,
      style: Theme.of(context).textTheme.bodyText2,
      textAlignVertical: TextAlignVertical.center,
      textAlign: TextAlign.left,
      validator: (value) {
        value = value?.trim() ?? '';
        if (!this.regexConfig.getRegExp.hasMatch(value)) {
          return this.regexConfig.errorText;
        }
        if (confirmController != null && confirmController.text.trim().compareTo(value) != 0) {
          return 'Password confirmation mismatched';
        }
        return null;
      },
      inputFormatters: TextInputType.multiline != keyboardType
        ? [
          LengthLimitation(maxLength: this.maxLength),
          FilteringTextInputFormatter.allow(RegExp(r'.')),
        ]
        : [
          LengthLimitation(maxLength: this.maxLength),
        ],
      minLines: minLines,
      maxLines: maxLines,
      decoration: InputDecoration(
        filled: false,
        hintText: this.hintText,
        contentPadding: const EdgeInsets.fromLTRB(3, 6, 3, 10),
        disabledBorder: _setInputBorder(color: Theme.of(context).disabledColor.withOpacity(0.8)),
        enabledBorder: _setInputBorder(color: Theme.of(context).cursorColor.withOpacity(0.3)),
        focusedBorder: _setInputBorder(color: Theme.of(context).cursorColor.withOpacity(0.8)),
        errorBorder: _setInputBorder(color: Theme.of(context).errorColor.withOpacity(0.6)),
        focusedErrorBorder: _setInputBorder(color: Theme.of(context).errorColor),
        border: _setInputBorder(color: Colors.yellow),
        suffixIcon: this.suffixIcon,
        prefixIcon: this.prefixIcon,
        isDense: true,
        errorMaxLines: 3,
        suffixIconConstraints: BoxConstraints(
          minHeight: 28,
          maxHeight: 36,
        ),
        prefixIconConstraints: BoxConstraints(
          minHeight: 28,
          maxHeight: 36,
        ),
        hintStyle: Theme.of(context).textTheme.bodyText1,
        errorStyle: Theme.of(context).textTheme.caption.copyWith(
          color: Theme.of(context).errorColor,
          fontStyle: FontStyle.italic,
        ),
      ),
    );
  }

  _setInputBorder({ @required Color color, double width }) {
    switch (this.borderType) {
      case BorderType.UNDERLINE:
        return UnderlineInputBorder(
          borderSide: BorderSide( width: width ?? 1, color: color, style: BorderStyle.solid),
        );
      case BorderType.OUTLINE:
        return OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide( width: width ?? 1, color: color, style: BorderStyle.solid),
        );
      case BorderType.NONE:
        return InputBorder.none;
      default:
        return UnderlineInputBorder(
          borderSide: BorderSide( width: width ?? 1, color: color, style: BorderStyle.solid),
        );
    }
  }

}