import 'package:blog/utils/snackbar-builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavigatorService {

  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  NavigatorService() {
    print('init navigator service');
  }

  Future<dynamic> pushNamed(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  Future<dynamic> push(MaterialPageRoute materialPageRoute) {
    return navigatorKey.currentState.push(materialPageRoute);
  }

  Future<dynamic> pushReplacementNamed(String routeName) {
    return navigatorKey.currentState.pushReplacementNamed(routeName);
  }

  Future<dynamic> pushNamedAndRemoveUntil(String routeName, { dynamic arguments, }) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(
      routeName,
      (_) => false,
      arguments: arguments,
    );
  }

  void pop() {
    return navigatorKey.currentState.pop();
  }

  BuildContext getContext() {
    return navigatorKey.currentState.overlay.context;
  }

  void showSnackBar(String message) {
    SnackBarBuilder.showSnackBar(
      getContext(),
      message: message,
    );
  }
}
