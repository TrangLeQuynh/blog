import 'dart:io';
import 'package:blog/config/constants.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:dio/dio.dart';

import 'locator.dart';

final NavigatorService _navigationService = locator<NavigatorService>();

Dio http = Dio(BaseOptions(
  connectTimeout: 600000,
  responseType: ResponseType.json,
  contentType: ContentType.json.toString(),),
)..interceptors.addAll([buildInterceptorsWrapper()]);

buildInterceptorsWrapper() {
  return InterceptorsWrapper(
    onRequest: (RequestOptions requestOptions) async {
      http.interceptors.requestLock.lock();
      requestOptions.headers[HttpHeaders.contentTypeHeader] = 'application/json';
      String token = await SharedPrefsService.getToken();
      if (token != null && token != '') {
        requestOptions.headers[HttpHeaders.authorizationHeader] = 'Bearer ' + token;
        http.interceptors.requestLock.unlock();
        return requestOptions;
      }
      http.interceptors.requestLock.unlock();
      return requestOptions;
    },
    onResponse: (Response response) async {
      try {
        ResponseData responseData = ResponseData.fromJson(response.data);
        response.data = responseData;
        return response;
      } on Exception catch (e) {
        return new ResponseData(false, 'System has some issues', null, null);
      }
    },
    onError: (DioError error) async {
      // Do something with response error
      if (error is Exception) {
        try {
          if (error is DioError) {
            print(error.type);
            switch (error.type) {
              case DioErrorType.CANCEL:
                print('requestCancelled');
                break;
              case DioErrorType.CONNECT_TIMEOUT:
                print('requestTimeout');
                break;
              case DioErrorType.DEFAULT:
                print('DioErrorType.DEFAULT');
                break;
              case DioErrorType.RECEIVE_TIMEOUT:
                print('sendTimeout');
                break;
              case DioErrorType.RESPONSE:
                print('response error');
                print(error.response);
                switch (error.response?.statusCode) {
                  case 400:
                    print('400');
                    break;
                  case 401:
                    Constants.isLoadingDialog = false;
                    _navigationService.pushNamedAndRemoveUntil(RoutingNameConstant.loginScreen);
                    _navigationService.showSnackBar('You need to login again');
                    break;
                  case 403:
                    print('403');
                    break;
                  case 404:
                    print('404');
                    break;
                  case 405:
                    print('405');
                    break;
                  default:
                    break;
                }
                break;
              case DioErrorType.SEND_TIMEOUT:
                break;
            }
          }
        } on FormatException catch (e) {
          print(e);
        } catch (e) {
          print(e);
        }
      }
      return error; //continue
    });
}

