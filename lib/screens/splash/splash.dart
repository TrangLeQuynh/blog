import 'package:blog/i18n/localization.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/providers/common/connectivity-provider.dart';
import 'package:blog/providers/common/language-provider.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/screens/setting/language-setting.dart';
import 'package:blog/screens/splash/widgets/page_view.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }

}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {

  ConnectivityResult oldConnectivityStatus;
  bool log = true;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      Provider.of<LanguageProvider>(context, listen: false).loadLanguageConfig();
      String code = await SharedPrefsService.getLanguageCode();
      await I18nLocalizations.of(context).loadByLocale(Locale(code));
      isLog().then((value) {
        if(this.mounted) {
          setState(() {
            log = value;
          });
        }
      });
    });
    super.initState();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: _buildBody(),
    );
  }

  Future<bool> isLog() async {
    String token = await SharedPrefsService.getToken();
    return token != '';
  }

  Future onConnected() async {
    print('on connect');
    bool res = await isLog();
    Provider.of<LanguageProvider>(context, listen: false).loadLanguageConfig();
    String code = await SharedPrefsService.getLanguageCode();
    await I18nLocalizations.of(context).loadByLocale(Locale(code));
    if (!res) return;
    User user = await Provider.of<UserProvider>(context, listen: false).getUser();
    if (user?.id == null) {
      SnackBarBuilder.showSnackBar(context, message: 'MSG_NEED_LOGIN_AGAIN');
      Navigator.of(context).pushReplacementNamed(RoutingNameConstant.loginScreen);
      return;
    }
    Navigator.of(context).pushReplacementNamed(RoutingNameConstant.homeScreen);
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 30,),
          _buildLanguageBox(context),
          const SizedBox(height: 30,),
          Expanded(
            child: PageViewer(),
          ),
          const SizedBox(height: 40,),
          _buildLoadConnectivity(context),
          const SizedBox(height: 60,),
        ],
      ),
    );
  }

  Widget _buildLoadConnectivity(BuildContext context) {
    return Consumer<ConnectivityProvider>(
      builder: (context, connectivityProvider, child) {
        var connectivityStatus = connectivityProvider.connectivityResult;
        if (connectivityStatus == null) {
          return SizedBox();
        }
        var isConnect = (connectivityStatus != ConnectivityResult.none);
        if (oldConnectivityStatus != connectivityStatus && isConnect) {
          onConnected();
        }
        oldConnectivityStatus = connectivityStatus;
        return !isConnect
        ? Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: SizedBox(
                height: 26,
                width: 26,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Text(
              I18nLocalizations?.of(context)?.trans('MSG_CHECK_CONNECTION') ?? '',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                color: Theme.of(context).hintColor,
              ),
            )
          ],
        )
        : _buildButtonToLogin(context);
      },
    );
  }

  Widget _buildLanguageBox(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: () {
          showModalBottomSheet(
            context: context,
            builder: (c) {
              return LanguageSettingView();
            },
          );
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 16,),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Theme.of(context).scaffoldBackgroundColor,
            border: Border.all(color: Theme.of(context).cursorColor, width: 0.6,),
          ),
          child:  Consumer<LanguageProvider>(
            builder: (c, model, child) {
              if (model.appLocal == null) return SizedBox();
              return  Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 22,
                    width: 22,
                    child: SvgPicture.asset(
                      'assets/images/flags/${model?.appLocal?.languageCode}.svg',
                      fit: BoxFit.contain,
                    ),
                  ),
                  const SizedBox(width: 10,),
                  Text(
                    UtilServices.textCapitalize(model?.appLocal?.languageCode),
                    style: Theme.of(context).textTheme.headline5.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildButtonToLogin(BuildContext context) {
    if (log) {
      return SizedBox(height: 20,);
    }
    return RaisedButton(
      onPressed: () {
        Navigator.of(context).pushReplacementNamed(RoutingNameConstant.loginScreen,);
      },
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Text(
          I18nLocalizations?.of(context)?.trans('BTN_LOGIN') ?? '',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );
  }

  // Widget _buildButtonOthers(BuildContext context) {
  //   return Row(
  //     children: [
  //       Expanded(
  //         flex: 1,
  //         child: flatButton(
  //           context,
  //           title: 'Facebook',
  //           image: 'assets/images/icons/ic_facebook.svg',
  //           textColor: Theme.of(context).canvasColor.withOpacity(0.6),
  //           borderColor: Theme.of(context).disabledColor,
  //           fillColor: Theme.of(context).backgroundColor,
  //           onPress: () {
  //             print('click Facebook');
  //             }
  //         ),
  //       ),
  //       const SizedBox(width: 12,),
  //       Expanded(
  //         flex: 1,
  //         child: flatButton(
  //           context,
  //           title: 'Google',
  //           image: 'assets/images/icons/ic_google.svg',
  //           textColor: Theme.of(context).canvasColor.withOpacity(0.6),
  //           borderColor: Theme.of(context).disabledColor,
  //           fillColor: Theme.of(context).backgroundColor,
  //           onPress: () {
  //             print('click Google');
  //           },
  //         ),
  //       ),
  //     ],
  //   );
  // }

}
