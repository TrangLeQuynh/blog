import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class PageViewer extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _PageViewerState();
  }

}

class _PageViewerState extends State<PageViewer> with SingleTickerProviderStateMixin {

  AnimationController _animationController;
  Animation<double> _animation;
  bool isReversing = false;
  int _currentPage = 0;
  final Duration _duration = Duration(milliseconds: 1200,);
  PageController _pageController = new PageController(initialPage: 0, );
  List<String> _liImage = [
    'assets/images/templates/page1.png',
    'assets/images/templates/page2.png',
    'assets/images/templates/page3.png',
  ];

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
      vsync: this,
      duration: _duration,
    );
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController);

    _animationController.addListener(() async {
      if (_animationController.status == AnimationStatus.completed) {
        _animationController.reset(); //Reset the controller
        await Future.delayed(Duration(milliseconds: 460));
        if (isReversing && _currentPage > 0) {
          _currentPage--;
        } else if (isReversing && _currentPage == 0) {
          _currentPage++;
          isReversing = false;
        } else if (_currentPage < _liImage.length - 1) {
          _currentPage++;
        } else {
          _currentPage--;
          isReversing = true;
        }
        _pageController?.animateToPage(_currentPage, duration: _duration, curve: Curves.linear);
      }
    });
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: _buildPageView(),
        ),
        const SizedBox(height: 20,),
        _buildIndicatorSmooth(),
      ],
    );
  }

  Widget _buildPageView() {
    return PageView.builder(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      onPageChanged: (index) {
        _animationController.forward();
      },
      allowImplicitScrolling: true,
      itemBuilder: (BuildContext context, int index) {
        return SizedBox(
          child: Image.asset(_liImage[index], fit: BoxFit.contain,),
          width: double.infinity - 38,
        );
      },
      itemCount: _liImage.length,
      reverse: false,
      scrollDirection: Axis.horizontal,
    );
  }

  Widget _buildIndicatorSmooth() {
    return SmoothPageIndicator(
      controller: _pageController,
      count: _liImage.length,
      axisDirection: Axis.horizontal,
      effect: WormEffect(
        dotColor: Theme.of(context).disabledColor.withOpacity(0.2),
        activeDotColor: Theme.of(context).accentColor,
        paintStyle: PaintingStyle.fill,
        strokeWidth: 1.2,
        dotHeight: 10,
        dotWidth: 10,
        radius: 10,
      ),
    );
  }

}
