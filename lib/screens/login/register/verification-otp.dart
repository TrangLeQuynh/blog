import 'package:blog/i18n/localization.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/service/auth-service.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class Verification extends StatefulWidget {

  @override
  VerificationState createState() => VerificationState();

}

class VerificationState extends State<Verification> {

  GlobalKey<FormState> _changePassKey = new GlobalKey();
  TextEditingController _verifyCodeController = TextEditingController();
  bool isInvisibleCurrent = true;
  bool isInvisibleNew = true;
  bool enable = false;
  num id;


  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      resend();
    });
    _verifyCodeController.addListener(() {
      setState(() {
        if (_verifyCodeController.text.length < 6)
          enable = false;
        else
          enable = true;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme
        .of(context)
        .backgroundColor,
      appBar: AppBarBuilder.baseAppBar(
        context,
        title: I18nLocalizations.of(context).trans('BTN_VERIFY_CODE'),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
        child: SingleChildScrollView(
          child: Form(
            key: _changePassKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 30,),
                SizedBox(
                  width: MediaQuery
                    .of(context)
                    .size
                    .width,
                  child: Image.asset(
                    'assets/images/icons/otp.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                const SizedBox(height: 35,),
                _buildPinCode(context),
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: () {
                      resend();
                    },
                    child: Text(
                      I18nLocalizations.of(context).trans('MSG_RESEND'),
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                  ),
                ),
                const SizedBox(height: 35,),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: !enable
                    ? null
                    : () {
                      _handleConfirmVerify();
                    },
                    child: Text(
                      I18nLocalizations.of(context).trans('BTN_CONFIRM'),
                      style: Theme.of(context).textTheme.button,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPinCode(BuildContext context) {
    return PinCodeTextField(
      appContext: context,
      length: 6,
      pinTheme: PinTheme(
        borderRadius: BorderRadius.circular(10),
        borderWidth: 1,
        disabledColor: Theme.of(context).scaffoldBackgroundColor,
        activeFillColor: Theme.of(context).scaffoldBackgroundColor,
        activeColor: Theme.of(context).cursorColor,
        inactiveColor: Theme.of(context).scaffoldBackgroundColor,
        inactiveFillColor: Theme.of(context).scaffoldBackgroundColor,
        selectedFillColor: Theme.of(context).scaffoldBackgroundColor,
        selectedColor: Theme.of(context).cursorColor,
        fieldHeight: 50,
        fieldWidth: 40,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      textStyle: Theme.of(context).textTheme.headline3.copyWith(fontWeight: FontWeight.w600,),
      animationDuration: Duration(milliseconds: 300),
      enableActiveFill: true,
      controller: _verifyCodeController,
      keyboardType: TextInputType.number,
    );
  }

  void resend() {
    setState(() {
      id = null;
    });
    AuthService.sendVerify(ModalRoute.of(context).settings.arguments as String).then((response) {
      if (response.status) {
        setState(() {
          id = response.data;
        });
      } else {
        SnackBarBuilder.showSnackBar(context, message: response.message,);
      }
    });
  }

  void _handleConfirmVerify() async {
    if (id == null) {
      SnackBarBuilder.showSnackBar(context, message: 'FAIL');
    }
    Map<String, dynamic> formData = {
      'id' : id,
      'verify_code' : _verifyCodeController.text,
    };
    DialogBuilder.buildLoadingDialog();
    ResponseData responseData = await AuthService.confirmVerifyRegister(formData);
    UtilServices.closeLoadingDialog(context);
    if (responseData.status) {
      DialogBuilder.buildNotificationDialog(
        urlImage: 'assets/images/icons/ic_access.svg',
        message: I18nLocalizations.of(context).trans('MSG_ACCOUNT_ACTIVATION_SUCCESS'),
        color: Theme.of(context).cursorColor,
        barrierDismissible: false,
      ).then((value) {
        Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.loginScreen, (route) => false);
      });
    } else {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
  }
}