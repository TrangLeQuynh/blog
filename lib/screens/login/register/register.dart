import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/service/auth-service.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/backgrounds/background_gradient.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {

  @override
  _RegisterState createState() => _RegisterState();

}

class _RegisterState extends State<RegisterScreen> {

  GlobalKey<FormState> _globalKeyRegister = new GlobalKey();
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _confirmPassController = new TextEditingController();
  bool isAutoValidate = false;

  @override
  void dispose() {
    _confirmPassController.dispose();
    _emailController.dispose();
    _passController.dispose();
    _usernameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          const Positioned(left: 0, bottom: 0, child: BackgroundGradient(),),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 35, horizontal: 46,),
            child: Center(
              child: SingleChildScrollView(
                child: _buildBody(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Form(
      key: _globalKeyRegister,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 30,),
          Image.asset('assets/images/logo/logo_gradient.png',
            width: 86,
            height: 86,),
          const SizedBox(height: 20,),
          Text(
            I18nLocalizations.of(context).trans('MSG_WELCOME_TO_GFIRE'),
            style: Theme.of(context).textTheme.headline1
          ),
          Text(
            I18nLocalizations.of(context).trans('BTN_REGISTER'),
            style: Theme.of(context).textTheme.headline2,
          ),
          const SizedBox(height: 35,),
          BasicTextField(
            controller: _usernameController,
            hintText: I18nLocalizations.of(context).trans('TXT_USERNAME'),
            regexConfig: RegexConstant.notEmpty,
            maxLength: 100,
            textInputFormatter: r'[\w-]',
            autoValidate: isAutoValidate,
          ),
          const SizedBox(height: 25,),
          BasicTextField(
            controller: _emailController,
            hintText: I18nLocalizations.of(context).trans('TXT_EMAIL'),
            regexConfig: RegexConstant.email,
            maxLength: 100,
            textInputFormatter: r'[^\s]',
            autoValidate: isAutoValidate,
          ),
          const SizedBox(height: 25,),
          BasicTextField(
            controller: _passController,
            hintText: I18nLocalizations.of(context).trans('TXT_PASSWORD'),
            regexConfig: RegexConstant.password,
            maxLength: 50,
            isPassword: true,
            textInputFormatter: r'[^\s]',
            autoValidate: isAutoValidate,
          ),
          const SizedBox(height: 25,),
          BasicTextField(
            hintText: I18nLocalizations.of(context).trans('TXT_CONFIRM_PASSWORD'),
            regexConfig: RegexConstant.password,
            controller: _confirmPassController,
            confirmController: _passController,
            maxLength: 50,
            isPassword: true,
            textInputFormatter: r'[^\s]',
            autoValidate: isAutoValidate,
          ),
          const SizedBox(height: 35,),
          RaisedButton(
            onPressed: () {
              _handleRegister();
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                I18nLocalizations.of(context).trans('BTN_REGISTER'),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.button,
              ),
            ),
          ),
          _buildLinkToLogin(),
          const SizedBox(height: 160,),
        ],
      ),
    );
  }

  Widget _buildLinkToLogin() {
    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 0),
        color: Colors.transparent,
        width: double.infinity,
        alignment: Alignment.centerRight,
        child: Text(
          I18nLocalizations.of(context).trans('MSG_BACK_TO_LOGIN'),
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
      onTap: () {
        Navigator.of(context).pop();
      },
    );
  }

  Future<bool> _handleRegister() async {
    if (!_globalKeyRegister.currentState.validate()) {
      setState(() {
     isAutoValidate = true;
      });
      return false;
    }
    setState(() {
      isAutoValidate = false;
    });
    DialogBuilder.buildLoadingDialog();
    Map<String, dynamic> formData = {
      'username' : _usernameController.text,
      'email' : _emailController.text,
      'password' : _passController.text
    };
    ResponseData responseData = await AuthService.register(formData);
    UtilServices.closeLoadingDialog(context);
    SnackBarBuilder.showSnackBar(
      context,
      message: responseData.message,
      status: responseData.status,
    );
    if (responseData.status)
      Navigator.of(context).pushNamed(
        RoutingNameConstant.verifyScreen,
        arguments: _emailController.text,
      );
    return responseData.status;
  }

}
