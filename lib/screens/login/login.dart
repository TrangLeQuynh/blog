import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/service/auth-service.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/backgrounds/background_gradient.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginState createState() => _LoginState();

}

class _LoginState extends State<LoginScreen> {

  GlobalKey<FormState> _globalKeyLogin = new GlobalKey();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  bool _isPassword = true;
  bool isAutoValidate = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          const Positioned(left: 0, bottom: 0, child: BackgroundGradient(),),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 35, horizontal: 46,),
            child: Column(
              children: [
                Expanded(
                  child: Center(
                    child: SingleChildScrollView(
                      child: _buildBodyLogin(),
                    ),
                  ),
                ),
                _buildLinkToRegister(),
              ]
            ),
          ),
        ]
      ),
    );
  }

  Widget _buildBodyLogin() {
    return Form(
      key: _globalKeyLogin,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 30,),
          Image.asset(
            'assets/images/logo/logo_gradient.png',
            width: 86,
            height: 86,
          ),
          const SizedBox(height: 20,),
          Text(
            I18nLocalizations.of(context).trans('MSG_WELCOME_TO_GFIRE'),
            style: Theme.of(context).textTheme.headline1,
          ),
          Text(
            I18nLocalizations.of(context).trans('BTN_LOGIN'),
            style: Theme.of(context).textTheme.headline2,
          ),
          const SizedBox(height: 35,),
          BasicTextField(
            controller: _emailController,
            hintText: I18nLocalizations.of(context).trans('TXT_EMAIL'),
            regexConfig: RegexConstant.email,
            maxLength: 100,
            textInputFormatter: r'[^\s]',
            autoValidate: isAutoValidate,
          ),
          const SizedBox(height: 25,),
          BasicTextField(
            controller: _passwordController,
            hintText: I18nLocalizations.of(context).trans('TXT_PASSWORD'),
            regexConfig: RegexConstant.password,
            maxLength: 50,
            autoValidate: isAutoValidate,
            suffixIcon: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                child: _isPassword
                  ? SvgPicture.asset(
                    'assets/images/icons/ic_invisible.svg',
                    fit: BoxFit.contain,
                    height: 18,
                    color: Theme.of(context).hintColor,
                  )
                  : SvgPicture.asset(
                    'assets/images/icons/ic_visibility.svg',
                    fit: BoxFit.contain,
                    height: 18,
                    color: Theme.of(context).cursorColor,
                  ),
              ),
              onTap: () {
                setState(() {
                  _isPassword = !_isPassword;
                });
              },
            ),
            textInputFormatter: r'[^\s]',
            isPassword: _isPassword,
          ),
          const SizedBox(height: 35,),
          RaisedButton(
            onPressed: () {
              _handleLogin(context);
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                I18nLocalizations.of(context).trans('BTN_LOGIN'),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.button,
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {
                print('Click Forgot password?');
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20,),
                child: Text(
                  I18nLocalizations.of(context).trans('TXT_FORGOT_PASSWORD'),
                  style: Theme.of(context).textTheme.subtitle2,
                ),
              ),
            ),
          ),
          const SizedBox(height: 160,),
        ],
      ),
    );
  }

  Widget _buildLinkToRegister() {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(
          RoutingNameConstant.registerScreen,
        );
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10,),
        color: Colors.transparent,
        width: double.infinity,
        alignment: Alignment.center,
        child: RichText(
          text: TextSpan(
            text: I18nLocalizations.of(context).trans('TXT_DONT_HAVE_ACCOUNT') + ' ',
            style: Theme.of(context).textTheme.bodyText1,
            children: <TextSpan>[
              TextSpan(
                text: I18nLocalizations.of(context).trans('BTN_REGISTER'),
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ],
          ),
        ),
      )
    );
  }

  void _handleLogin(BuildContext context) async {
    if (!_globalKeyLogin.currentState.validate()) {
      setState(() {
        isAutoValidate = true;
      });
      return;
    }
    setState(() {
      isAutoValidate = false;
    });
    Map<String, String> formData = {
      'email' : _emailController.text,
      'password' : _passwordController.text
    };
    DialogBuilder.buildLoadingDialog();
    ResponseData responseData = await AuthService.getToken(formData);
    if (responseData.status) {
      await Provider.of<UserProvider>(context, listen: false).getUser();
    }
    UtilServices.closeLoadingDialog(context);
    if (responseData.status) {
      SnackBarBuilder.showSnackBar(
        context,
        message: responseData.message,
        status: true,
      );
      Navigator.of(context).pushReplacementNamed(
        RoutingNameConstant.homeScreen
      );
    } else {
      SnackBarBuilder.showSnackBar(
        context,
        message: responseData.message,
      );
    }
  }

}