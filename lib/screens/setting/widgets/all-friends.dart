import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/user/friend.dart';
import 'package:blog/screens/chat/chat-room.dart';
import 'package:blog/service/friend-service.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AllFriends extends StatefulWidget {

  @override
  AllFriendState createState() => AllFriendState();

}

class AllFriendState extends State<AllFriends> {

  TextEditingController _searchController = new TextEditingController();
  List<Friend> liFriend = [];
  bool loading = true;

  Future<void> _refresh() async {
    FriendService.getFriendList(_searchController.text).then((value) {
      setState(() {
        loading = false;
      });
      if (value.status) {
        setState(() {
          liFriend = value.data ?? [];
        });
      } else {
        SnackBarBuilder.showSnackBar(context, message: value.message);
      }
    });
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    _searchController.addListener(() {
      _refresh();
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _refresh();
    });
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _refresh,
      backgroundColor: Theme.of(context).backgroundColor,
      child: CustomScrollView(
        slivers: [
          _buildSearchItem(),
          _buildListFriend(),
        ],
      ),
    );
  }

  SliverToBoxAdapter _buildSearchItem() {
    return SliverToBoxAdapter(
      child: Container(
        margin: const EdgeInsets.fromLTRB(20, 16, 20, 5),
        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 15,),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: BasicTextField(
          regexConfig: RegexConstant.none,
          controller: _searchController,
          borderType: BorderType.NONE,
          hintText: I18nLocalizations.of(context).trans('TXT_SEARCH_FRIEND'),
          prefixIcon: Padding(
            padding: const EdgeInsets.only(right: 10,),
            child: Icon(
              Icons.search,
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildItem(Friend item) {
    return GestureDetector(
      onTap: () {
        // print('xem chi tiet');
        Navigator.of(context).push(
          MaterialPageRoute(builder: (c) => ChatRoomScreen(sender: item.friend))
        );
      },
      child: Container(
        color: Colors.transparent,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AvatarWidget(
              size: 65,
              url: item.friend.avatar,
            ),
            const SizedBox(width: 10,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${item.friend.username}',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  const SizedBox(height: 6,),
                  Text(
                    '${item.friend.email}',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.clear,
                color: Theme.of(context).errorColor,
              ),
              onPressed: () async {
                bool res = await DialogBuilder.buildNotificationDialog(
                  urlImage: 'assets/images/icons/ic_delete.svg',
                  message: I18nLocalizations.of(context).trans('QA_DELETE_FRIEND'),
                  dialogType: DialogType.CONFIRM_CANCEL,
                );
                if (res) {
                  FriendService.responseFriend(item, false).then((responseData) {
                    if (responseData.status) {
                      SnackBarBuilder.showSnackBar(context, message: responseData.message, status: true);
                      _refresh();
                    } else {
                      SnackBarBuilder.showSnackBar(context, message: responseData.message);
                    }
                  });
                }
              },
            )
          ],
        ),
      ),
    );
  }

  _buildListFriend() {
    if (loading)
      return SliverToBoxAdapter(
        child: Center(
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 160,),
            height: 32,
            width: 32,
            child: CircularProgressIndicator(strokeWidth: 2.6,),
          ),
        ),
      );
    if (liFriend.length == 0)
      return SliverToBoxAdapter(
        child: Center(
          child: EmptyBox(),
        ),
      );
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (c, index) {
          return _buildItem(liFriend[index]);
        },
        childCount: liFriend.length,
      ),
    );
  }

}