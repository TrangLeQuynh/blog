import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/service/friend-service.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddFriends extends StatefulWidget {

  @override
  AddFriendState createState() => AddFriendState();

}

class AddFriendState extends State<AddFriends> {

  TextEditingController _searchNewFriend = new TextEditingController();
  List<User> liFriend = [];

  Future<void> _refresh() async {
    if (_searchNewFriend.text.trim() == '') {
      setState(() {
        liFriend = [];
      });
    }
    FriendService.findNewFriendList(_searchNewFriend.text).then((value) {
      if (value.status) {
        setState(() {
          liFriend = value.data ?? [];
        });
      } else {
        SnackBarBuilder.showSnackBar(context, message: value.message);
      }
    });
  }

  @override
  void initState() {
    _searchNewFriend.addListener(() {
      _refresh();
    });
    super.initState();
  }

  @override
  void dispose() {
    _searchNewFriend.dispose();
    liFriend.clear();
    super.dispose();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _refresh,
      backgroundColor: Theme.of(context).backgroundColor,
      child: CustomScrollView(
        slivers: [
          _buildSearchItem(),
          _buildListFriend(),
        ],
      ),
    );
  }

  SliverToBoxAdapter _buildSearchItem() {
    return SliverToBoxAdapter(
      child: Container(
        margin: const EdgeInsets.fromLTRB(20, 16, 20, 5),
        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 15,),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: BasicTextField(
          regexConfig: RegexConstant.none,
          controller: _searchNewFriend,
          borderType: BorderType.NONE,
          hintText: I18nLocalizations.of(context).trans('TXT_SEARCH_FRIEND'),
          prefixIcon: Padding(
            padding: const EdgeInsets.only(right: 10,),
            child: Icon(
              Icons.search,
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildItem(User item) {
    return GestureDetector(
      onTap: () {
        print('xem chi tiet');
      },
      child: Container(
        color: Colors.transparent,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AvatarWidget(
              size: 65,
              url: item.avatar,
            ),
            const SizedBox(width: 10,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${item.username}',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  const SizedBox(height: 6,),
                  Text(
                    '${item.email}',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.add,
                color: Theme.of(context).cursorColor,
                size: 28,
              ),
              onPressed: () async {
                bool res = await DialogBuilder.buildNotificationDialog(
                  urlImage: 'assets/images/icons/ic_add_friend.svg',
                  message: I18nLocalizations.of(context).trans('QA_ADD_FRIEND'),
                  dialogType: DialogType.CONFIRM_CANCEL,
                );
                if (res) {
                  _handleAddFriend(context, item.id);
                }
              },
            )
          ],
        ),
      ),
    );
  }

  _buildListFriend() {
    if (liFriend.length == 0)
      return SliverToBoxAdapter(
        child: Center(
          child: EmptyBox(),
        ),
      );
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (c, index) {
          return _buildItem(liFriend[index]);
        },
        childCount: liFriend.length,
      ),
    );
  }

  void _handleAddFriend(BuildContext context, num friendId) async {

    DialogBuilder.buildLoadingDialog();
    Map<String, dynamic> formData = {
      'userId' : Provider.of<UserProvider>(context, listen: false).user.id,
      'friendId' : friendId
    };
    ResponseData responseData = await FriendService.addFriend(formData);
    UtilServices.closeLoadingDialog(context);
    if (responseData.status) {
      _refresh();
    }
    SnackBarBuilder.showSnackBar(context, message: responseData.message, status: responseData.status);

  }

}