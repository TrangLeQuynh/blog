import 'package:blog/i18n/localization.dart';
import 'package:blog/models/user/friend.dart';
import 'package:blog/service/friend-service.dart';
import 'package:blog/utils/date_converter.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

class FriendRequest extends StatefulWidget {

  @override
  FriendRequestState createState() => FriendRequestState();

}

class FriendRequestState extends State<FriendRequest> {

  bool loading = true;
  List<Friend> liFriend = [];
  
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      _refresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _refresh,
      backgroundColor: Theme.of(context).backgroundColor,
      child: _buildListRequestFriend(),
    );
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  Future<void> _refresh() async {
    FriendService.getRequestList().then((value) {
      setState(() {
        loading = false;
      });
      if (value.status) {
        setState(() {
          liFriend = value.data ?? [];
        });
      } else {
        SnackBarBuilder.showSnackBar(context, message: value.message);
      }
    });
    //call api
  }

  Widget _buildItem(Friend friend) {
    return GestureDetector(
      onTap: () {
        print('xem chi tiet');
      },
      child: Container(
        color: Colors.transparent,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AvatarWidget(
              size: 65,
              url: friend.user.avatar,
            ),
            const SizedBox(width: 10,),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      Expanded(child: Text(
                        '${friend.user.username}',
                        style: Theme.of(context).textTheme.headline4,
                      ),),
                      Expanded(child: Text(
                        '${DateConverter.convertDate(friend.createAt)}',
                        textAlign: TextAlign.end,
                        style: Theme.of(context).textTheme.caption,
                      ),)
                    ],
                  ),
                  const SizedBox(height: 3,),
                  _buildButtons(friend),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButtons(Friend item) {
    return Row(
      children: [
        Expanded(
          child: MaterialButton(
            height: 0,
            padding: const EdgeInsets.symmetric(vertical: 6.5,),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            color: Theme.of(context).cursorColor,
            onPressed: () {
              FriendService.responseFriend(item, true).then(
                (responseData) {
                  if (responseData.status) {
                    SnackBarBuilder.showSnackBar(context, message: responseData.message, status: true);
                    _refresh();
                  } else {
                    SnackBarBuilder.showSnackBar(context, message: responseData.message);
                  }
              });
            },
            child: Text(
              'Accept',
              style: Theme.of(context).textTheme.button,
            ),
          ),
        ),
        const SizedBox(width: 8,),
        Expanded(
          child: MaterialButton(
            height: 0,
            padding: const EdgeInsets.symmetric(vertical: 6.5,),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            color:  Theme.of(context).disabledColor.withOpacity(0.6),
            onPressed: () async {
              bool res = await DialogBuilder.buildNotificationDialog(
                urlImage: 'assets/images/icons/ic_delete.svg',
                message: I18nLocalizations.of(context).trans('QA_REJECT_FRIEND'),
                dialogType: DialogType.CONFIRM_CANCEL,
              );
              if (res) {
                FriendService.responseFriend(item, false).then((responseData) {
                  if (responseData.status) {
                    SnackBarBuilder.showSnackBar(context, message: responseData.message, status: true);
                    _refresh();
                  } else {
                    SnackBarBuilder.showSnackBar(context, message: responseData.message);
                  }
                });
              }
            },
            child: Text(
              'Delete',
              style: Theme.of(context).textTheme.button,
            ),
          ),
        ),
      ],
    );
  }

  _buildListRequestFriend() {
    if (loading)
      return Center(
        child: SizedBox(
          height: 32,
          width: 32,
          child: CircularProgressIndicator(strokeWidth: 2.6,),
        ),
      );
    if (liFriend.length == 0)
      return Center(
        child: EmptyBox(),
      );
    return ListView.builder(
      itemCount: liFriend.length,
      itemBuilder: (c, index) {
        return _buildItem(liFriend[index]);
      },
    );
  }

}