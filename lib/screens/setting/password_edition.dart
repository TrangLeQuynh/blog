import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/service/user-service.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PasswordEdition extends StatefulWidget {

  @override
  PasswordEditionState createState() => PasswordEditionState();

}

class PasswordEditionState extends State<PasswordEdition> {

  GlobalKey<FormState> _changePassKey = new GlobalKey();
  // TextEditingController _currentPassController = new TextEditingController();
  TextEditingController _newPassController = new TextEditingController();
  TextEditingController _confirmPassController = new TextEditingController();

  bool isInvisibleCurrent = true;
  bool isInvisibleNew = true;

  @override
  void dispose() {
    _newPassController.dispose();
    // _currentPassController.dispose();
    _confirmPassController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBarBuilder.baseAppBar(
        context,
        title: I18nLocalizations.of(context).trans('BTN_CHANGE_PASSWORD'),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(35, 20, 35, 0),
        child: SingleChildScrollView(
          child: Form(
            key: _changePassKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 30,),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Image.asset(
                    'assets/images/icons/security.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                const SizedBox(height: 35,),
                // BasicTextField(
                //   controller: _currentPassController,
                //   regexConfig: RegexConstant.password,
                //   borderType: BorderType.UNDERLINE,
                //   hintText: 'Current password',
                //   isPassword: isInvisibleCurrent,
                //   suffixIcon: _buildSuffixIcon(
                //     isInvisible: isInvisibleCurrent,
                //     function: () {
                //       setState(() {
                //         isInvisibleCurrent = !isInvisibleCurrent;
                //       });
                //     },
                //   ),
                // ),
                // const SizedBox(height: 25,),
                BasicTextField(
                  controller: _newPassController,
                  regexConfig: RegexConstant.password,
                  borderType: BorderType.UNDERLINE,
                  hintText: I18nLocalizations.of(context).trans('TXT_NEW_PASSWORD'),
                  isPassword: isInvisibleNew,
                  suffixIcon: _buildSuffixIcon(
                    isInvisible: isInvisibleNew,
                    function: () {
                      setState(() {
                        isInvisibleNew = !isInvisibleNew;
                      });
                    },
                  ),
                ),
                const SizedBox(height: 25,),
                BasicTextField(
                  controller: _confirmPassController,
                  regexConfig: RegexConstant.password,
                  borderType: BorderType.UNDERLINE,
                  confirmController: _newPassController,
                  isPassword: true,
                  hintText: I18nLocalizations.of(context).trans('TXT_CONFIRM_PASSWORD'),
                ),
                const SizedBox(height: 35,),
                RaisedButton(
                  onPressed: () async {
                    bool result = await _handleChangePassword();
                    if (result) {
                      DialogBuilder.buildNotificationDialog(
                        urlImage: 'assets/images/icons/ic_access.svg',
                        message: I18nLocalizations.of(context).trans('MSG_CHANGE_PASS_SUCCESS'),
                        color: Theme.of(context).cursorColor,
                        barrierDismissible: false,
                      ).then((value) {
                        UtilServices.logout(context);
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          RoutingNameConstant.loginScreen,
                          (route) => false,
                        );
                      });
                    }
                  },
                  child: Text(
                    I18nLocalizations.of(context).trans('BTN_CHANGE_PASSWORD'),
                    style: Theme.of(context).textTheme.button,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSuffixIcon({ @required bool isInvisible, @required VoidCallback function, }) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        child: isInvisible
        ? SvgPicture.asset(
          'assets/images/icons/ic_invisible.svg',
          fit: BoxFit.contain,
          height: 18,
          color: Theme.of(context).hintColor,
        )
            : SvgPicture.asset(
          'assets/images/icons/ic_visibility.svg',
          fit: BoxFit.contain,
          height: 18,
          color: Theme.of(context).cursorColor,
        ),
      ),
      onTap: function,
    );
  }

  Future<bool> _handleChangePassword() async {
    if (!_changePassKey.currentState.validate())
      return false;
    bool res = await DialogBuilder.buildNotificationDialog(
      urlImage: 'assets/images/icons/ic_pass_confirm.svg',
      message: I18nLocalizations.of(context).trans('QA_CHANGE_PASSWORD'),
      color: Theme.of(context).cursorColor,
      dialogType: DialogType.CONFIRM_CANCEL,
    );
    if (!res) return res;
    print(_newPassController.text);
    print(_confirmPassController.text);
    // if (_newPassController.text.trim().compareTo(_currentPassController.text.trim()) == 0) {
    //   DialogBuilder.buildNotificationDialog(
    //     urlImage: 'assets/images/icons/ic_pass_confirm.svg',
    //     color: Theme.of(context).cursorColor,
    //     message: 'Old Password and new password cannot be same',
    //     barrierDismissible: false,
    //   );
    //   return false;
    // }
    DialogBuilder.buildLoadingDialog();
    ResponseData responseData = await UserService.changePassword(_newPassController.text);
    UtilServices.closeLoadingDialog(context);
    if (!responseData.status) {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
    return responseData.status;
  }

}
