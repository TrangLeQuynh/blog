import 'package:blog/i18n/localization.dart';
import 'package:blog/models/common/language-config.dart';
import 'package:blog/providers/common/language-provider.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class LanguageSettingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 38),
        constraints: BoxConstraints(
          maxHeight: 320,
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                I18nLocalizations.of(context).trans('BTN_LANGUAGE'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ),
              ),
              const SizedBox(height: 20,),
              Consumer<LanguageProvider>(
                builder: (c, language, child) {
                  return ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (c, index) {
                      bool _res = language.appLocal.languageCode == language.languageConfigs[index].code;
                      String _code = language.languageConfigs[index].code;
                      return GestureDetector(
                        onTap: () async {
                          if (_res) {
                            Navigator.of(context).pop();
                            return;
                          }
                          await I18nLocalizations.of(context).loadByLocale(Locale(_code));
                          language.loadNewLanguageSuccess(_code);
                          Navigator.of(context).pop();
                        },
                        child: _buildItem(
                          context: context,
                          item: language.languageConfigs[index],
                          status: _res,
                        ),
                      );
                    },
                    separatorBuilder: (c, index) {
                      return Divider();
                    },
                    itemCount: language.languageConfigs.length,
                  );
                },
              )
            ],
          ),
    );
  }

  Widget _buildItem({
    @required BuildContext context,
    @required LanguageConfig item,
    @required bool status,
  }) {
    return Container(
        color: Colors.transparent,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 22,
              width: 22,
              child: SvgPicture.asset(
                'assets/images/flags/${item.flag}.svg',
                fit: BoxFit.contain,
              ),
            ),
            const SizedBox(width: 20,),
            Expanded(
              child: Text(
                item.name ?? '',
                style: Theme.of(context).textTheme.headline5.copyWith(
                  fontWeight: FontWeight.w300
                ),
              ),
            ),
            Icon(
              Icons.check,
              color: status
                ? Theme.of(context).accentColor
                : Colors.transparent,
            ),
          ],
      ),
    );
  }
}

