import 'package:blog/i18n/localization.dart';
import 'package:blog/screens/setting/widgets/add-friend.dart';
import 'package:blog/screens/setting/widgets/all-friends.dart';
import 'package:blog/screens/setting/widgets/friend_request.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FriendListScreen extends StatefulWidget {

  @override
  FriendListState createState() => FriendListState();

}

class FriendListState extends State<FriendListScreen> {

  PageController pageController = new PageController(initialPage: 1);
  int currentIndex = 1;

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBarBuilder.baseAppBar(
        context,
        title: I18nLocalizations.of(context).trans('BTN_FRIENDS'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //chips
          _buildHeader(context),
          Divider(),
          Expanded(child: _buildBody(context)),
        ],
      ),
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildChip(I18nLocalizations.of(context).trans('BTN_REQUEST'), 0),
            _buildChip(I18nLocalizations.of(context).trans('BTN_ALL_FRIEND'), 1),
            _buildChip(I18nLocalizations.of(context).trans('BTN_ADD_FRIEND'), 2),
          ],
        ),
      ),
    );
  }

  Widget _buildChip(String label, int index) {
    return GestureDetector(
      onTap: () {
        if (index != currentIndex)
          pageController.jumpToPage(index);
      },
      child: Container(
        child: Text(
          label,
          style: Theme.of(context).textTheme.headline6,
        ),
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20,),
        margin: const EdgeInsets.only(right: 8,),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            width: 1.0,
            color: index == currentIndex
              ? Theme.of(context).cursorColor
              : Theme.of(context).disabledColor.withOpacity(0.6),
          ),
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        setState(() {
          currentIndex = index;
        });
      },
      children: <Widget>[
        FriendRequest(),
        AllFriends(),
        AddFriends(),
      ],
    );
  }

}
