import 'package:blog/i18n/localization.dart';
import 'package:blog/models/post/post.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/screens/profile/widgets/header-view.dart';
import 'package:blog/screens/profile/widgets/images_view.dart';
import 'package:blog/screens/profile/widgets/post-list.dart';
import 'package:blog/widgets/items/drawer-view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {

  @override
  ProfileState createState() => ProfileState();

}

class ProfileState extends State<ProfileScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<Post> liPost = [];

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<UserProvider>(context, listen: false).getUser();
    });
    _tabController = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Theme.of(context).backgroundColor,
      drawer: DrawerView(),
      floatingActionButton: null,
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return <Widget> [
            SliverToBoxAdapter(
              child: Consumer<UserProvider>(
                builder: (context, model, child) {
                  return HeaderView(
                    user: model.user,
                  );
                },
              ),
            ),
            SliverAppBar(
              snap: true,
              elevation: 0,
              pinned: true,
              floating: true,
              toolbarHeight: 0,
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).backgroundColor,
              bottom: _buildHeaderTabBar(),
            ),
            //_buildTabBarHeader(),
          ];
        },
        body: TabBarView(
          children: [
            PostList(
              userId: Provider.of<UserProvider>(context, listen: false).user.id,
            ),
            ImagesView(
              userId: Provider.of<UserProvider>(context, listen: false).user.id,
            ),
          ],
          controller: _tabController,
        ),
      )
    );
  }

  TabBar _buildHeaderTabBar() {
    return TabBar(
      controller: _tabController,
      indicatorColor: Theme.of(context).accentColor,
      indicatorWeight: 3,
      indicatorPadding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
      labelStyle: Theme.of(context).textTheme.headline6,
      unselectedLabelStyle: Theme.of(context).textTheme.headline6,
      labelColor: Theme.of(context).textTheme.bodyText1.color,
      unselectedLabelColor: Theme.of(context).hintColor,
      tabs: [
        Tab(
          text: I18nLocalizations.of(context).trans('BTN_POST'),
        ),
        Tab(
          text: I18nLocalizations.of(context).trans('TXT_IMAGE'),
        )
      ],
    );
  }

  Widget _buildAction() {
    return Container(
      child: Icon(Icons.add, color: Colors.white,),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Theme.of(context).accentColor,
        border: Border.all(color: Theme.of(context).backgroundColor, width: 2,)
      ),
    );
  }
}

