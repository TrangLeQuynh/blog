import 'package:blog/models/response/response_data.dart';
import 'package:blog/service/image-service.dart';
import 'package:blog/widgets/images/image-network.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'ImagesSlider.dart';

class ImagesView extends StatefulWidget {

  final num userId;

  const ImagesView({ @required this.userId, });

  @override
  _ImagesViewState createState() => _ImagesViewState();

}

class _ImagesViewState extends State<ImagesView> {

  static const double _endReachedThreshold = 100;
  final ScrollController _controller = ScrollController();
  Map<String, dynamic> param = {
    'page' : 0,
    'size' : 16,
  };
  List<String> liImages = [];
  bool isLoading = true;
  bool _canLoadMore = true;

  @override
  void initState() {
    _controller.addListener(_onScroll);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _refresh();
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      backgroundColor: Theme.of(context).backgroundColor,
      child: isLoading && liImages.isEmpty
        ? Center(
          child: SizedBox(
            height: 32,
            width: 32,
            child: CircularProgressIndicator(strokeWidth: 2.6,),
          ),
        )
        : liImages.isNotEmpty
          ? _buildBody()
          : EmptyBox(),
      onRefresh: _refresh,
    );
  }

  Widget _buildBody() {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: StaggeredGridView.countBuilder(
        controller: _controller,
        mainAxisSpacing: 2,
        crossAxisSpacing: 2,
        crossAxisCount: 3,
        itemCount: liImages.length,
        itemBuilder: (c, index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext c) => ImagesSlider(
                    liImage: liImages,
                    initialIndex: index,
                  ),
                ),
              );
            },
            child: imageNetwork(
              url: liImages[index],
              boxFit: BoxFit.cover,
            ),
          );
        },
        staggeredTileBuilder: (index) {
          if (index == 0)
            return StaggeredTile.count(3, 1.5);
          if (index % 7 == 0)
            return StaggeredTile.count(2, 2);
          return StaggeredTile.count(1, 1);
        },
      ),
    );
  }

  void _onScroll() {
    if(isLoading || !_canLoadMore)
      return;
    final thresholdReached = _controller.position.extentAfter < _endReachedThreshold;
    if(thresholdReached) {
      load(param['page'] + 1);
    }
  }

  Future<void> _refresh() async {
    liImages.clear();
    load(0);
  }

  Future<void> load(int page) async {
    setState(() {
      isLoading = true;
    });
    param['page'] = page;
    ResponseData responseData = await ImageService.getListImage(param);
    List<String> li = responseData.data ?? [];
    if (li.length < param['size']) {
      _canLoadMore = false;
    }
    liImages.addAll(li);
    isLoading = false;
    setState(() { });
    return;
  }
}
