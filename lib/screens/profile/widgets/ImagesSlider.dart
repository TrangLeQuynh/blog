import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/widgets/images/image-network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImagesSlider extends StatefulWidget {

  List<String> liImage;
  int initialIndex;

  ImagesSlider({ @required this.liImage, @required this.initialIndex });

  @override
  ImagesSliderState createState() => ImagesSliderState();

}

class ImagesSliderState extends State<ImagesSlider> {

  PageController _pageController;

  @override
  void initState() {
    _pageController = new PageController(initialPage: widget.initialIndex, );
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black45,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.black45,
        toolbarHeight: 30,
      ),
      body: _buildPageView(),
    );
  }


  Widget _buildPageView() {
    return PageView.builder(
      controller: _pageController,
      allowImplicitScrolling: true,
      itemBuilder: (BuildContext context, int index) {
        return imageNetwork(
          url: widget.liImage[index],
          boxFit: BoxFit.contain,
        );
      },
      itemCount: widget.liImage.length,
      scrollDirection: Axis.horizontal,
    );
  }

}
