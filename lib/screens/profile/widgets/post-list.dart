import 'package:blog/models/post/post.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/screens/posts/post-detail.dart';
import 'package:blog/screens/posts/post-view.dart';
import 'package:blog/screens/posts/upload-post.dart';
import 'package:blog/service/post-service.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:blog/widgets/items/loadmore-delegate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PostList extends StatefulWidget {

  num userId;
  PostList({ @required this.userId, });

  @override
  _PostListState createState() => _PostListState();

}

class _PostListState extends State<PostList> {

  List<Post> liPost = [];
  Map<String, dynamic> param = {
    'page' : 0,
    'size' : 12,
  };
  static const double _endReachedThreshold = 100;
  final ScrollController _controller = ScrollController();
  bool _canLoadMore = true;
  bool loading = true;

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    print(widget.userId);
    _controller.addListener(_onScroll);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _refresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading && liPost.isEmpty) {
      return Center(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 160,),
          height: 32,
          width: 32,
          child: CircularProgressIndicator(strokeWidth: 2.6,),
        ),
      );
    }
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: CustomScrollView(
        //physics: NeverScrollableScrollPhysics(),
        controller: _controller,
        slivers: [
          SliverToBoxAdapter(
            child: Padding(
              child: PostUpload(
                function: () {
                  _refresh();
                },
              ),
              padding: const EdgeInsets.fromLTRB(20, 25, 20, 3,),
            ),
          ),
          liPost?.isEmpty ?? true
          ? SliverToBoxAdapter(
            child: Center(
              child: EmptyBox(),
            ),
          )
          : SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,),
                  child: PostView(
                    post: liPost[index],
                    isOwner: true,
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) => PostDetailScreen(
                          postId: liPost[index].id,
                          post: liPost[index],
                        ))
                      ).then((value) {
                        _refresh();
                      });
                    },
                  ),
                );
              },
              childCount: liPost.length,
            ),
          ),
          SliverToBoxAdapter(
            child: LoadMoreDelegate(status: _canLoadMore,),
          ),
        ],
      ),
    );
  }

  void _onScroll() {
    if(loading || !_canLoadMore)
      return;
    final thresholdReached = _controller.position.extentAfter < _endReachedThreshold;
    if(thresholdReached) {
      print('load more');
      load(param['page'] + 1);
    }
  }

  Future<void> _refresh() async {
    liPost.clear();
    load(0);
  }

  Future<void> load(int page) async {
    setState(() {
      loading = true;
    });
    param['page'] = page;
    ResponseData responseData = await PostService.getListPost(param, userId: widget.userId);
    List<Post> li = responseData.data ?? [];
    if (li.length < param['size']) {
      _canLoadMore = false;
    }
    liPost.addAll(li);
    loading = false;
    setState(() { });
    if (!responseData.status) {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
    return;
  }
}
