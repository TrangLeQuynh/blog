import 'dart:io';
import 'package:blog/config/constants.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/service/user-service.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/backgrounds/background_gradient.dart';
import 'package:blog/widgets/images/image-network.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/camera-view.dart';
import 'package:blog/widgets/items/upload-image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class HeaderView extends StatefulWidget {

  final User user;

  HeaderView({
    @required this.user,
  });

  @override
  HeaderState createState() => HeaderState();

}

class HeaderState extends State<HeaderView> {

  File file;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<UserProvider>(context, listen: false).getUser();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme
        .of(context)
        .backgroundColor,
      margin: const EdgeInsets.all(0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildHeaderImage(context),
          const SizedBox(height: 6,),
          Text(
            '@${widget.user?.username ?? ''}',
            style: Theme
              .of(context)
              .textTheme
              .headline4,
          ),
          const SizedBox(height: 2,),
          Text(
            '${widget.user?.email ?? ''}',
            style: Theme
                .of(context)
                .textTheme
                .bodyText1
                .copyWith(
              color: Theme
                  .of(context)
                  .hintColor,
            ),
          ),
          _buildNumberInfo(context),
        ],
      ),
    );
  }

  Widget _buildAvatar(BuildContext context, double sizeImage) {
    return Container(
      height: sizeImage,
      width: sizeImage,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        shape: BoxShape.circle,
        border: Border.all(color: Theme.of(context).backgroundColor, width: 5,),
      ),
      child: AvatarWidget(
        size: sizeImage,
        url: widget?.user?.avatar,
      ),
    );

  }

  Widget _buildHeaderImage(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double sizeImage = (size.width - 40) / 3;
    return Container(
      color: Theme.of(context).backgroundColor,
      height: sizeImage + 80,
      width: size.width,
      child: Stack(
        children: [
          Positioned(
            child: GestureDetector(
              child: Container(
                height: double.infinity,
                color: Theme.of(context).scaffoldBackgroundColor,
                child: widget?.user?.cover == null || widget.user.cover == ''
                  ? SizedBox()
                  : imageNetwork(
                    url: '${widget.user.cover}',
                    boxFit: BoxFit.cover,
                  ),
                padding: const EdgeInsets.only(bottom: 1,),
              ),
              onTap: () {
                uploadImage(
                  context,
                  onTapImage: () async {
                    Navigator.of(context).pop();
                    File value = await _openImage(fileType: FileType.image);
                    if (value == null) return;
                    await handleUploadCover(value);
                  },
                  onTapCamera: () async {
                    Navigator.of(context).pop();
                    File value = await _openCameraView();
                    if (value == null) return;
                    await handleUploadCover(value);
                  },
                );
              },
            ),
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
          ),
          Positioned(
            child: Container(
              height: sizeImage / 2 + 5,
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
            ),
            bottom: 0,
            left: 0,
            right: 0,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              child: _buildAvatar(context, sizeImage),
              onTap: () {
                uploadImage(
                  context,
                  onTapImage: () async {
                    Navigator.of(context).pop();
                    File value = await _openImage(fileType: FileType.image);
                    if (value == null) return;
                    await handleUploadAvatar(value);
                  },
                  onTapCamera: () async {
                    Navigator.of(context).pop();
                    File value = await _openCameraView();
                    if (value == null) return;
                    await handleUploadAvatar(value);
                  },
                );
              },
            ),
          )

        ],
      ),
    );
  }

  Widget _buildNumberInfo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 68, left: 68, top: 15,),
      child: Row(
        children: [
          Expanded(
            child: _buildItem(
              context,
              number: widget.user?.friends ?? 0,
              label: I18nLocalizations.of(context).trans('BTN_FRIENDS'),
            ),
          ),
          const SizedBox(width: 3,),
          SizedBox(
            height: 30,
            child: VerticalDivider(),
          ),
          const SizedBox(width: 3,),
          Expanded(
            child: _buildItem(
              context,
              number: widget.user?.posts ?? 0,
              label: I18nLocalizations.of(context).trans('BTN_POSTS'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, { @required num number, @required String label, }) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          '$number',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline4,
        ),
        const SizedBox(height: 3,),
        Text(
          label,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
            color: Theme.of(context).hintColor,
          ),
        ),
      ],
    );
  }

  Future<File> _openImage({ @required FileType fileType, }) async {
    try {
      FilePickerResult result = (await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.image,
      ));
      if (result == null) {
        SnackBarBuilder.showSnackBar(
          context,
          message: 'MSG_HAVENT_CHOSEN_FILES_YET'
        );
        return null;
      }
      if (!this.mounted) return null;
      File file = File(result.paths.first);
      return file;
    } on PlatformException catch (e) {
      print('platform exception ${e.toString()}');
      return null;
    }
  }

  Future<File> _openCameraView() async {
    String value = await Navigator.of(context).push(
        MaterialPageRoute(builder: (c) => CameraView())
    );
    if (value == null || value == '')
      return null;
    return File(value);
  }

  Future<void> handleUploadAvatar(File file) async {
    DialogBuilder.buildLoadingDialog();
    String base64 = await UtilServices.convertToBase64(file);
    ResponseData responseData = await UserService.updateAvatar(base64);
    UtilServices.closeLoadingDialog(context);
    if (responseData.status) {
      Provider.of<UserProvider>(context, listen: false).getUser();
    }
    SnackBarBuilder.showSnackBar(
      context,
      message: responseData.message,
      status: responseData.status
    );
  }

  Future<void> handleUploadCover(File file) async {
    DialogBuilder.buildLoadingDialog();
    String base64 = await UtilServices.convertToBase64(file);
    ResponseData responseData = await UserService.updateCover(base64);
    UtilServices.closeLoadingDialog(context);
    if (responseData.status) {
      Provider.of<UserProvider>(context, listen: false).getUser();
    }
    SnackBarBuilder.showSnackBar(
      context,
      message: responseData.message,
      status: responseData.status,
    );
  }

}
