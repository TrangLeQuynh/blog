import 'dart:math';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class NotificationScreen extends StatefulWidget {

  @override
  NotificationState createState() => NotificationState();

}

class NotificationState extends State<NotificationScreen> {

  Random random = new Random();
  int number;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        number = random.nextInt(60);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarBuilder.baseAppBar(
        context,
        title: 'Notification',
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 10,),
        child: RefreshIndicator(
          backgroundColor: Theme.of(context).backgroundColor,
          onRefresh: _refresh,
          child: _buildBody(context),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView.builder(
      itemBuilder: (c, index) {
        return _buildItem(
          context,
          username: 'Trang Súnoleo',
          content: 'đã nhắc bạn trong một bình luận',
        );
      },
      itemCount: 10,
    );
  }

  Widget _buildItem(BuildContext context, { @required String username, String content, }) {
    return Container(
      padding: const EdgeInsets.only(top: 20,),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildAvatar(),
          const SizedBox(width: 12,),
          Expanded(
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                children: [
                  //username
                  TextSpan(
                    text: '$username ',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  TextSpan(
                    text: content ?? '',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ]
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAvatar() {
    return Stack(
      children: [
        AvatarWidget(size: 54,),
        Positioned(
          bottom: 0,
          right: 0,
          child: SizedBox(
            height: 18,
            width: 18,
            child: SvgPicture.asset(
              random.nextBool()
                ? 'assets/images/icons/ic_heart_red.svg'
                : 'assets/images/icons/ic_comment_green.svg',
              fit: BoxFit.contain,
              alignment: Alignment.center,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _refresh() async {
    await Future.delayed(Duration(milliseconds: 1000,));
    if (!this.mounted) return;
    setState(() {
      number = random.nextInt(60);
    });
  }

}