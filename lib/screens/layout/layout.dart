import 'dart:convert';
import 'package:blog/config/config.dart';
import 'package:blog/config/constants.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/common/payload.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/screens/chat/chat.dart';
import 'package:blog/screens/home/home.dart';
import 'package:blog/screens/profile/profile.dart';
import 'package:blog/screens/setting/friend_list.dart';
import 'package:blog/service/notification-service.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';


class DashboardHomePage extends StatefulWidget {

  @override
  _DashboardHomePageState createState() => _DashboardHomePageState();
}

class _DashboardHomePageState extends State<DashboardHomePage> {
  int _selectedIndex = 0;

  List listPage = [ HomePage(), FriendListScreen(), ChatScreen(),  ProfileScreen(),];
  StompClient stompClient;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      int args = ModalRoute.of(context)?.settings?.arguments as int;
      if (args != null) {
        setState(() {
          _selectedIndex = args;
        });
      }
      connectSocket();
    });
    super.initState();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    stompClient?.deactivate();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listPage.elementAt(_selectedIndex),
      bottomNavigationBar:BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: buildIcon('ic_home_v1.svg'),
            activeIcon: buildIcon('ic_home_v1.svg', isActive: true),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: buildIcon('ic_add.svg'),
            activeIcon: buildIcon('ic_add.svg', isActive: true),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: buildIcon('ic_chat.svg'),
            activeIcon: buildIcon('ic_chat.svg', isActive: true),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: buildIcon('ic_user.svg'),
            activeIcon: buildIcon('ic_user.svg', isActive: true),
            label: '',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Theme.of(context).accentColor,
        unselectedItemColor: Theme.of(context).disabledColor,
        type: BottomNavigationBarType.fixed,
        onTap: _onItemTapped,
        elevation: 8,
        showUnselectedLabels: false,
        showSelectedLabels: false,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        backgroundColor: Theme.of(context).backgroundColor,
      ),
    );
  }

  Widget buildIcon(String path, {bool isActive = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 6,),
      child: SizedBox(
        height: 24,
        width: 22,
        child: SvgPicture.asset(
          'assets/images/icons/$path',
          alignment: Alignment.center,
          color: isActive
            ? Theme.of(context).cursorColor
            : Theme.of(context).disabledColor,
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void connectSocket() async {
    String token = await SharedPrefsService.getToken();
    stompClient = StompClient(
      config: StompConfig.SockJS(
        url: '${EnvConfig.SOCKET_URL}',
        onConnect: onConnect,
        onWebSocketError: (dynamic error) => print(error.toString()),
        stompConnectHeaders: {'Authorization': 'Bearer $token'},
        webSocketConnectHeaders: {'Authorization': 'Bearer $token'},
      ),
    );
    stompClient.activate();
  }

  void onConnect(StompClient client, StompFrame frame) {
    num userId = Provider.of<UserProvider>(context, listen: false).user.id;
    client.subscribe(
      destination: '/user/$userId/chatroom-list',
      callback: (StompFrame frame) {
        if (frame.body != null && frame.body != "" && !Constants.isShowChatScreen) {
          Map<String, dynamic> result = json.decode(frame.body);
          Payload payload = Payload.fromJson(result);
          NotificationService().showNotification(
            title: I18nLocalizations.of(context).trans('MSG_HAVE_NEW_MESSAGE'),
            subtitle: '${payload.senderName} ${I18nLocalizations.of(context).trans('MSG_CHAT_FOR_YOU')}',
            payload: frame.body,
          );
        }
      },
    );
    client.subscribe(
      destination: '/user/$userId/comment-list/notification',
      callback: (StompFrame frame) {
        if (frame.body != null && !Constants.isShowChatScreen) {
          Map<String, dynamic> result = json.decode(frame.body);
          Payload payload = Payload.fromJson(result);
          if (Constants.commentViewIndex == payload.id) return;
          NotificationService().showNotification(
            title: I18nLocalizations.of(context).trans('MSG_HAVE_NEW_NOTIFICATION'),
            subtitle: '${payload.senderName} ${I18nLocalizations.of(context).trans('MSG_COMMENT_YOUR_POST')}',
            payload: frame.body,
          );
        }
      },
    );
  }
}
