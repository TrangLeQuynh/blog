import 'package:blog/i18n/localization.dart';
import 'package:blog/models/post/post.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/screens/posts/post-edition.dart';
import 'package:blog/screens/profile/widgets/ImagesSlider.dart';
import 'package:blog/service/post-service.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/utils/date_converter.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/images/image-network.dart';
import 'package:blog/screens/posts/post_action.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class PostDetailScreen extends StatefulWidget {
  final num postId;
  final Post post;

  PostDetailScreen({ this.postId, this.post, });

  @override
  PostDetailState createState() => PostDetailState();

}

class PostDetailState extends State<PostDetailScreen> {

  Post postDetail;
  bool _loading = false;

  @override
  void initState() {
    if (widget.post != null) {
      setState(() {
        postDetail = widget.post;
      });
    }
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _refresh();
    });
    super.initState();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (postDetail == null && _loading) {
      return Scaffold(
        body: Center(
          child: SizedBox(
            height: 32,
            width: 32,
            child: CircularProgressIndicator(strokeWidth: 1.8,),
          ),
        ),
      );
    }
    if (postDetail == null) {
      return Scaffold(
        body: Center(
          child: EmptyBox(),
        ),
      );
    }
    return Scaffold(
      appBar: AppBarBuilder.baseAppBar(
        context,
        actions: postDetail?.user?.id != Provider.of<UserProvider>(context, listen: false).user.id
        ? null
        : [
          _buildPopupMenuButton(),
        ]
      ),
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              slivers: [
                postDetail.images.length == 0
                ? SliverToBoxAdapter()
                : _showImages(context),
                SliverToBoxAdapter(
                  child: _buildContent(context),
                ),
              ],
            ),
          ),
          Container(
            color: Theme.of(context).backgroundColor,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: PostAction(postDetail),
          ),
        ],
      )
    );
  }

  SliverStaggeredGrid _showImages(BuildContext context) {
    return new SliverStaggeredGrid.countBuilder(
      mainAxisSpacing: 3.0,
      crossAxisSpacing: 3.0,
      crossAxisCount: 2,
      itemCount: postDetail.images.length,
      itemBuilder: (c, index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext c) => ImagesSlider(
                  liImage: postDetail.images,
                  initialIndex: index,
                ),
              ),
            );
          },
          child: imageNetwork(
            url: postDetail.images[index],
            boxFit: BoxFit.cover,
          ),
        );
      },
      staggeredTileBuilder: (index) {
        if (postDetail.images.length == 1)
          return StaggeredTile.count(2,1.5);
        if (postDetail.images.length == 2)
          return StaggeredTile.count(1, 1.5);
        if (index == 0 && postDetail.images.length % 2 != 0)
          return StaggeredTile.count(1, 2);
        return StaggeredTile.count(1,1);
      }
    );
  }

  Widget _buildContent(context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTitle(context),
          const SizedBox(height: 20,),
          // Text(
          //   'Demons',
          //   style: Theme.of(context).textTheme.headline5,
          // ),
          // const SizedBox(height: 3,),
          // Text(
          //   '#Demons #Imagine_Dragons',
          //   style: Theme.of(context).textTheme.subtitle1,
          // ),
          // const SizedBox(height: 3,),
          Text(
            postDetail.content,
            textAlign: TextAlign.justify,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(height: 25,),
        ],
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Row(
      children: [
        AvatarWidget(
          size: 60,
          url: postDetail.user.avatar,
        ),
        const SizedBox(width: 10,),
        Expanded(
          child: Text(
            '${postDetail.user.username}',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Text(
          '${DateConverter.convertDate(postDetail.createAt,)}',
          style: Theme.of(context).textTheme.caption,
        ),
      ],
    );
  }

  void _refresh() async {
    _loading = true;
    ResponseData responseData = await PostService.getPostDetail(widget.postId);
    if (responseData.status) {
      postDetail = responseData.data;
    } else {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
    _loading = false;
    setState(() { });
  }

  void _handleDelete() async {
    bool res = await DialogBuilder.buildNotificationDialog(
      urlImage: 'assets/images/icons/ic_delete.svg',
      message: I18nLocalizations.of(context).trans('QA_DELETE_THE_POST'),
      color: Theme.of(context).cursorColor,
      dialogType: DialogType.CONFIRM_CANCEL,
    );
    if (!res) return;
    DialogBuilder.buildLoadingDialog();
    ResponseData responseData = await PostService.deletePost(widget.postId);
    UtilServices.closeLoadingDialog(context);
    SnackBarBuilder.showSnackBar(
      context,
      message: responseData.message,
      status: responseData.status
    );
    if (responseData.status) {
      Navigator.of(context).pop();
    }

  }

  Widget _buildPopupMenuButton() {
    return PopupMenuButton(
      elevation: 8,
      offset: Offset(0, 6),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        color: Colors.transparent,
        padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        child: SvgPicture.asset(
          'assets/images/icons/ic_menu_dots.svg',
          fit: BoxFit.contain,
          color: Theme.of(context).appBarTheme.iconTheme.color,
        ),
      ),
      itemBuilder: (c) => [
        _buildPopupItem(
          image: 'assets/images/icons/ic_edit.svg',
          title: I18nLocalizations.of(context).trans('TXT_EDIT'),
          color: Theme.of(context).cursorColor,
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (C) {
                return PostEditionScreen(
                  canEdit: true,
                  post: postDetail,
                );
              })
            ).then((value) {
              _refresh();
            });

          },
        ),
        _buildPopupItem(
          image: 'assets/images/icons/ic_delete_close.svg',
          title: I18nLocalizations.of(context).trans('TXT_DELETE'),
          color: Theme.of(context).errorColor,
          onTap: () {
            _handleDelete();
          },
        )
      ],
    );
  }

  PopupMenuItem _buildPopupItem({
    @required String image,
    @required Function onTap,
    @required String title,
    Color color,
  }) {
    return PopupMenuItem(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
          onTap();
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25,),
          color: Colors.transparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 20,
                height: 20,
                child: SvgPicture.asset(
                  '$image',
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  color: color,
                ),
              ),
              const SizedBox(width: 25,),
              Text(
                '$title',
                style: Theme.of(context).textTheme.bodyText2.copyWith(
                  fontSize: 16,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}
