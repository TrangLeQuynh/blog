import 'dart:io';
import 'package:blog/config/constants.dart';
import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/models/post/image-file.dart';
import 'package:blog/models/post/post.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/service/post-service.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/utils/date_converter.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/camera-view.dart';
import 'package:file_picker/file_picker.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class PostEditionScreen extends StatefulWidget {

  final bool canEdit;
  final Post post;

  PostEditionScreen({ this.canEdit = false, this.post });

  @override
  PostEditionState createState() => PostEditionState();

}

class PostEditionState extends State<PostEditionScreen> {

  Location _location = new Location();
  PermissionStatus permission;
  bool enableUpload = false;
  TextEditingController _contentController = new TextEditingController();
  List<ImageFile> files = [];
  LocationData _locationData;

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    _contentController.addListener(_changeActionUpload);
    if (widget.canEdit) {
      setState(() {
        _contentController.text = widget.post.content;
      });
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        List<ImageFile> tmpLi = [];
        for (String item in widget.post.images) {
          tmpLi.add(new ImageFile(link: item, type: false));
        }
        setState(() {
          files.addAll(tmpLi);
        });
      });
    }
    _location.hasPermission().then((value) {
      permission = value;
    });
    super.initState();
  }

  void _changeActionUpload() {
    if (_contentController.text.trim() != ''){
      setState(() {
        enableUpload = true;
      });
    } else {
      setState(() {
        enableUpload = false;
      });
    }
  }

  Future<bool> checkPermission(PermissionStatus permissionStatus) async {
    switch (permissionStatus) {
      case PermissionStatus.denied:
        PermissionStatus res = await _location.requestPermission();
        setState(() {
          permission = res;
        });
        return res == PermissionStatus.granted || res == PermissionStatus.grantedLimited;
      case PermissionStatus.deniedForever:
        return false;
      case PermissionStatus.granted:
        return true;
      case PermissionStatus.grantedLimited:
        return true;
      default:
        return false;
    }
  }

  @override
  void dispose() {
    _contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(widget.canEdit ? 'TXT_EDIT_THE_POST' : 'TXT_CREATE_THE_POST'),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              child: _buildContent(context),
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20,),
            )
          ),
          _buildWidgetOverlap(MediaQuery.of(context).viewInsets.bottom),
        ],
      ),
    );
  }

  AppBar _buildAppBar(String title) {
    return AppBarBuilder.baseAppBar(
      context,
      title: I18nLocalizations.of(context).trans(title),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 6, top: 8, bottom: 8,),
          child: RaisedButton(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
            disabledTextColor: Theme.of(context).disabledColor,
            textColor: Colors.white,
            child: Text(
              I18nLocalizations.of(context).trans('BTN_UPLOAD'),
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            onPressed: enableUpload
              ? () {
                _handleUpload();
              }
              : null,
          ),
        ),
      ],
    );
  }

  Widget _buildContent(BuildContext context) {
    double sizeImg = (MediaQuery.of(context).size.width - 70)/3;
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Consumer<UserProvider>(
            builder: (context, model, child) {
              return _buildHeader(context, model.user);
            },
          ),
          const SizedBox(height: 20,),
          BasicTextField(
            hintText: I18nLocalizations.of(context).trans('TXT_WHAT_ARE_YOU_THINK'),
            regexConfig: RegexConstant.notEmpty,
            controller: _contentController,
            maxLines: 20,
            minLines: 1,
            maxLength: 265000,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            borderType: BorderType.NONE,
          ),
          const SizedBox(height: 20,),
          Wrap(
            children: List.generate(files.length, (index) {
              return Container(
                height: sizeImg,
                width: sizeImg,
                margin: const EdgeInsets.only(right: 10, bottom: 10,),
                alignment: Alignment.topRight,
                decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor.withOpacity(0.3),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: files[index].type
                      ? FileImage(
                        new File(files[index].link ?? ''),
                      )
                      : NetworkImage(
                        '${Constants.URL_IMAGE}${files[index].link}'
                      ),
                  ),
                ),
                child: GestureDetector(
                  child: Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Theme.of(context).canvasColor.withOpacity(0.3)
                    ),
                    child: Icon(
                      Icons.close,
                      color: Theme.of(context).backgroundColor,
                      size: 24,
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      files.removeAt(index);
                    });
                  },
                ),
              );
            })
          )
        ],
      ),
    );
  }

  Widget _buildHeader(BuildContext context, User user) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        AvatarWidget(
          size: 42,
          url: user.avatar,
        ),
        const SizedBox(width: 10,),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${user?.username ?? ''}',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: Theme.of(context).textTheme.bodyText2,
              ),
              Text(
                '${DateConverter.convertDate(widget.post?.createAt ?? DateTime.now().millisecondsSinceEpoch)}',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          )
        ),

      ],
    );
  }

  Widget _buildWidgetOverlap(double bottom) {
    if (bottom == 0.0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildAction(
            imageLink: 'assets/images/system/ic_image.svg',
            title: I18nLocalizations.of(context).trans('TXT_IMAGE'),
            function: () {
              _openFiles(fileType: FileType.image);
            },
          ),
          _buildAction(
            imageLink: 'assets/images/system/ic_camera.svg',
            title: I18nLocalizations.of(context).trans('TXT_CAMERA'),
            function: _openCameraView,
          ),
          _buildAction(
            imageLink: 'assets/images/system/ic_location.svg',
            title: I18nLocalizations.of(context).trans('TXT_LOCATION'),
            function: _getLocation,
          ),
        ],
      );
    }
    return InkWell(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10,),
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border: Border(
            top: BorderSide(
              width: 0.36,
              color: Theme.of(context).dividerColor,
            ),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Text(
                I18nLocalizations.of(context).trans('TXT_ADD_YOUR_POST'),
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            const SizedBox(width: 10,),
            _buildIcon('assets/images/system/ic_image.svg',),
            const SizedBox(width: 18,),
            _buildIcon('assets/images/system/ic_camera.svg',),
            const SizedBox(width: 18,),
            _buildIcon('assets/images/system/ic_location.svg',),
          ],
        ),
      ),
    );
  }

  Widget _buildAction({
    @required String imageLink,
    @required String title,
    Color color,
    GestureTapCallback function,
  }) {
    return GestureDetector(
      onTap: function,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20,),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border(
            top: BorderSide(
              width: 0.35,
              color: Theme.of(context).dividerColor,
            ),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildIcon(imageLink),
            const SizedBox(width: 15,),
            Expanded(
              child: Text(
                title,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildIcon(String imageLink, { Color color }) {
    return SizedBox(
      height: 30,
      width: 28,
      child: SvgPicture.asset(
        '$imageLink',
        fit: BoxFit.contain,
        color: color,
        alignment: Alignment.center,
      ),
    );
  }

  void _openFiles({ @required FileType fileType, }) async {
    try {
      FilePickerResult result = (await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.image,
      ));
      if (result == null) {
        SnackBarBuilder.showSnackBar(
          context,
          message: 'MSG_HAVENT_CHOSEN_FILES_YET'
        );
        return;
      }
      setState(() {
        files.addAll(result.paths.map((item) => ImageFile(link: item)).toList());
      });

    } on PlatformException catch (e) {
      print('platform exception ${e.toString()}');
    }
  }

  void _openCameraView() async {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (c) => CameraView())
    ).then((value) {
      if (value != null && value != '') {
        setState(() {
          files.add(value);
        });
      }
    });
  }

  void _getLocation() async {
    bool status = await checkPermission(permission);
    if (!status)
      return;
    if (_locationData == null) {
      print('permission ${permission.toString()}');
      DialogBuilder.buildLoadingDialog();
      LocationData locationData = await _location.getLocation();
      setState(() {
        _locationData = locationData;
      });
      UtilServices.closeLoadingDialog(context);
    }
  }

  void _handleUpload() async {
    List<String> liImage = await getListImage();
    if (liImage.length > 5) {
      DialogBuilder.buildNotificationDialog(
        barrierDismissible: true,
        color: Theme.of(context).cursorColor,
        urlImage: 'assets/images/icons/ic_notification.svg',
        message: I18nLocalizations.of(context).trans('MSG_LIMIT_IMAGE_NUMBER_UPLOAD'),
      );
      return;
    }
    Map<String, dynamic> formData = {
      'id' : widget?.post?.id,
      'content' : _contentController.text,
      'images' : liImage,
      'user_id' : Provider.of<UserProvider>(context, listen: false).user.id,
      'latitude' : _locationData?.latitude ?? '',
      'longitude' : _locationData?.longitude ?? '',
    };
    DialogBuilder.buildLoadingDialog();
    ResponseData responseData = await PostService.editPost(formData, widget.canEdit ?? false);
    UtilServices.closeLoadingDialog(context);
    SnackBarBuilder.showSnackBar(
      context,
      message: responseData.message,
      status: responseData.status
    );
    if (responseData.status) {
      //push success
      Navigator.of(context).pop();
    }
  }

  Future<List<String>> getListImage() async {
    List<String> li = [];
    String base64;
    for(int i = 0; i < files.length; ++i) {
      if (files[i].type) {
        base64 = await UtilServices.convertToBase64(File(files[i].link));
        li.add(base64);
      } else {
        li.add(files[i].link);
      }
    }
    return li;
  }

}
