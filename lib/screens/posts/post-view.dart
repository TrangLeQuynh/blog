import 'package:blog/models/post/post.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/utils/date_converter.dart';
import 'package:blog/screens/posts/post_action.dart';
import 'package:blog/styles/styles.dart';
import 'package:blog/widgets/images/image-network.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostView extends StatefulWidget {

  final Post post;
  final bool isOwner;
  VoidCallback onTap;

  PostView({ @required this.post, @required this.onTap, this.isOwner = false, });

  @override
  PostViewState createState() => PostViewState();
}

class PostViewState extends State<PostView> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        padding: const EdgeInsets.all(3),
        margin: const EdgeInsets.fromLTRB(3, 10, 3, 5,),
        decoration: BoxDecoration(
          boxShadow: [
            CommonStyles.getShadow(context),
          ],
          color: Theme.of(context).cardTheme.color,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: Column(
            children: [
              widget.post.images != null && widget.post.images.length != 0
              ? _buildImage(context)
              : SizedBox(),
              const SizedBox(height: 10,),
              _buildTitle(context),
              _buildContent(context),
              const Divider(
                indent: 20,
                endIndent: 20,
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 8,
                ),
                child: PostAction(widget.post),
              )
            ],
          ),
      ),
    );
  }

  Widget _buildImage(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(15)),
      child: SizedBox(
        height: 165,
        width: MediaQuery.of(context).size.width,
        child: imageNetwork(
          url: widget.post.images.first,
          boxFit: BoxFit.cover,
        ),
      )
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 10,),
        AvatarWidget(
          url: widget.isOwner
            ? Provider.of<UserProvider>(context, listen: true).user.avatar
            : widget.post.user.avatar,
        ),
        const SizedBox(width: 10,),
        Expanded(
          child: Text(
            widget.post.user.username,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Text(
          DateConverter.convertDate(widget.post.createAt),
          style: Theme.of(context).textTheme.caption,
        ),
        const SizedBox(width: 10,),
      ],
    );
  }

  Widget _buildContent(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8,),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Text(
          //   'Demons',
          //   style: Theme.of(context).textTheme.headline5,
          // ),
          // const SizedBox(height: 3,),
          // Text(
          //   '#Demons #Imagine_Dragons',
          //   style: Theme.of(context).textTheme.subtitle1,
          // ),
          // const SizedBox(height: 3,),
          Text(
            widget.post.content ?? '',
            maxLines: 4,
            textAlign: TextAlign.justify,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }

}