import 'package:blog/models/post/post.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'comment-view.dart';

class PostAction extends StatefulWidget {

  final Post post;
  PostAction(this.post);

  @override
  PostActionState createState() => PostActionState();

}

class PostActionState extends State<PostAction> {

  bool isHeart = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _buildItemAction(
          context,
          num: '',
          imageDisable: 'ic_heart.svg',
          imageActive: 'ic_heart_red.svg',
          isActive: isHeart,
          onTap: () {
            setState(() {
              isHeart = !isHeart;
            });
          }
        ),
        const SizedBox(width: 10,),
        _buildItemAction(
            context,
            num: '',
            imageDisable: 'ic_comment.svg',
            imageActive: 'ic_heart_red.svg',
            isActive: false,
            onTap: () {
              showCommentView(context, widget.post);
            }
        ),
        const SizedBox(width: 10,),
        _buildItemAction(
          context,
          num: '',
          imageDisable: 'ic_share.svg',
          imageActive: 'ic_heart_red.svg',
          isActive: false,
        ),
      ],
    );
  }

  Widget _buildItemAction(BuildContext context,{
    String num = '',
    @required String imageDisable,
    @required String imageActive,
    @required bool isActive,
    GestureTapCallback onTap,
  }) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(width: 5,),
            Text( //can be overflow
              num,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.right,
              style: Theme.of(context).textTheme.bodyText2?.copyWith(fontSize: 12,),
            ),
            const SizedBox(width: 5,),
            SizedBox(
              width: 22,
              height: 22,
              child: isActive
                ? SvgPicture.asset(
                  'assets/images/icons/$imageActive',
                  fit: BoxFit.contain,
                )
                : SvgPicture.asset(
                  'assets/images/icons/$imageDisable',
                  fit: BoxFit.contain,
                  color: Theme.of(context).textTheme.bodyText1?.color,
                ),
            ),
          ],
        ),
      ),
    );
  }

}
