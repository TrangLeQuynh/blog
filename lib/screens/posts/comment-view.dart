import 'dart:convert';

import 'package:blog/config/config.dart';
import 'package:blog/config/constants.dart';
import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/post/comment.dart';
import 'package:blog/models/post/post.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/service/comment-service.dart';
import 'package:blog/utils/date_converter.dart';
import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/utils/utils.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

class CommentView extends StatefulWidget {

  final Post post;

  CommentView(this.post);

  @override
  CommentState createState() => CommentState();

}

class CommentState extends State<CommentView> {

  num userID;
  List<Comment> liComment = [];
  bool loading = true;
  TextEditingController _commentController = new TextEditingController();
  ScrollController _scrollController = new ScrollController();
  bool canComment = false;
  StompClient stompClient;

  @override
  void initState() {
    Constants.commentViewIndex = widget.post.id;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      connectSocket();
      userID = Provider.of<UserProvider>(context, listen: false).user.id;
      _refresh();
    });
    super.initState();
  }

  @override
  void dispose() {
    Constants.commentViewIndex = -1;
    stompClient?.deactivate();
    _commentController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: _buildList(),
        ),
        Divider(),
        _enterComment(context),
      ],
    );
  }

  Widget _buildList() {
    if (loading) {
      return Center(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 160,),
          height: 32,
          width: 32,
          child: CircularProgressIndicator(strokeWidth: 2.6,),
        ),
      );
    }
    return ListView.builder(
      controller: _scrollController,
      itemBuilder: (_, index) {
        if (liComment[index].user.id == userID || widget.post.user.id == userID) {
          return Dismissible(
            key: Key('${liComment[index].id}-$userID'),
            child: _buildItem(
              context,
              (index == liComment.length - 1),
              liComment[index],
            ),
            background:Container(
              child: SvgPicture.asset(
                'assets/images/icons/ic_delete_close.svg',
                color: Colors.white,
                height: 15,
                alignment: Alignment.centerRight,
              ),
              color: Theme.of(context).errorColor,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 28,),
            ),
            onDismissed: (direction) {
              setState(() {
                liComment.removeAt(index);
              });
            },
            confirmDismiss: (direction) async {
              return await _handleDeleteComment(index);
            },
            crossAxisEndOffset: 0,
            direction: DismissDirection.endToStart,
          );
        }
        return _buildItem(
          context,
          (index == liComment.length - 1),
          liComment[index],
        );
      },
      itemCount: liComment.length,
    );
  }

  Widget _buildItem(BuildContext context, bool isLast, Comment item) {
    return Column(
      children: [
        const SizedBox(height: 20,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(width: 20,),
            AvatarWidget(
              url: item.user.avatar,
              size: 46,
            ),
            const SizedBox(width: 12,),
            Expanded(
              child:Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                            '${item.user.username}',
                            style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      const SizedBox(width: 12,),
                      Text(
                        '${DateConverter.convertDate(item.createAt)}',
                        style: Theme.of(context).textTheme.caption,
                      )
                    ],
                  ),
                  const SizedBox(height: 10,),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                        '${item.content}',
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 20,),
          ],
        ),
        const SizedBox(height: 20,),
        isLast
          ? SizedBox()
          : Divider(
            indent: 78,
            endIndent: 20,
            height: 1,
          ),
      ],
    );
  }

  Widget _enterComment(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20,),
        child: Row(
          children: [
            Expanded(
              child: BasicTextField(
                controller: _commentController,
                hintText: I18nLocalizations.of(context).trans('TXT_WRITE_A_COMMENT'),
                borderType: BorderType.NONE,
                regexConfig: RegexConstant.notEmpty,
              ),
            ),
            GestureDetector(
              onTap: () {
                _addComment();
              },
              child: SizedBox(
                width: 32,
                height: 32,
                child: SvgPicture.asset(
                  'assets/images/icons/ic_send_v1.svg',
                  fit: BoxFit.contain,
                  color: Theme.of(context).cursorColor,
                  alignment: Alignment.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _addComment() async {
    String value = _commentController.text.trim();
    if (value.length == 0) return;
    DialogBuilder.buildLoadingDialog();
    Map<String, dynamic> formData = {
      'content' : value,
      'postId' : widget.post.id,
      'userId' : userID,
      'ownerId' : widget.post.user.id,
    };
    stompClient.send(destination: "/app/comment-add", body: jsonEncode(formData),);
    UtilServices.closeLoadingDialog(context);
    _commentController.clear();
    _refresh();
  }

  Future<void> _reload() async {
    ResponseData responseData = await CommentService.getListComment(widget.post.id);
    if (!responseData.status) return;
    setState(() {
      liComment = responseData.data ?? [];
    });
  }

  Future<void> _refresh() async {
    _commentController.clear();
    ResponseData responseData = await CommentService.getListComment(widget.post.id);
    setState(() {
      loading = false;
    });
    if (!this.mounted) return;
    if (!responseData.status) {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
    setState(() {
      liComment = responseData.data ?? [];
    });
  }

  Future<bool> _handleDeleteComment(num index) async {
    DialogBuilder.buildLoadingDialog();
    Map<String, dynamic> formData = {
      'postId' : widget.post.id,
      'id' : liComment[index].id,
    };
    stompClient.send(destination: "/app/comment-delete", body: jsonEncode(formData),);
    UtilServices.closeLoadingDialog(context);
    liComment.removeAt(index);
    setState(() {});
  }

  void connectSocket() async {
    String token = await SharedPrefsService.getToken();
    stompClient = StompClient(
      config: StompConfig.SockJS(
        url: '${EnvConfig.SOCKET_URL}',
        onConnect: onConnect,
        onWebSocketError: (dynamic error) => print(error.toString()),
        stompConnectHeaders: {'Authorization': 'Bearer $token'},
        webSocketConnectHeaders: {'Authorization': 'Bearer $token'},
      ),
    );
    stompClient.activate();
  }

  void onConnect(StompClient client, StompFrame frame) {
    client.subscribe(
      destination: '/user/${widget.post.id}/comment-post',
      callback: (StompFrame frame) {
        print('frame.body ${frame.body}');
        if (frame.body != null) {
          Map<String, dynamic> result = json.decode(frame.body);
          Comment msg = Comment.fromJson(result);
          liComment.add(msg);
          setState(() { });
          _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
          _reload();
        }
      },
    );
    client.subscribe(
      destination: '/user/${widget.post.id}/delete/comment-post',
      callback: (StompFrame frame) {
        if (frame.body != null) {
          _refresh();
          _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
        }
      },
    );
  }
}

void showCommentView(BuildContext context, Post post) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    useRootNavigator: true,
    builder: (_) {
      return FractionallySizedBox(
        heightFactor: 0.86,
        child: Column(
          children: [
            Padding(
              child: Text(
                I18nLocalizations.of(context).trans('BTN_COMMENTS'),
                style: Theme.of(context).textTheme.headline6,
              ),
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25,),
            ),
            Divider(),
            Expanded(child: CommentView(post),),
          ],
        ),
      );
    }
  );
}
