import 'package:blog/i18n/localization.dart';
import 'package:blog/providers/common/language-provider.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/styles/styles.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class PostUpload extends StatefulWidget {

  VoidCallback function;

  PostUpload({ @required this.function, });

  @override
  PostState createState() => PostState();

}

class PostState extends State<PostUpload> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(RoutingNameConstant.postEditionScreen).then((value) {
          widget.function();
        });
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10,),
        margin: const EdgeInsets.all(3,),
        decoration: BoxDecoration(
          boxShadow: [
            CommonStyles.getShadow(context),
          ],
          color: Theme.of(context).cardTheme.color,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 3),
                  child:  Consumer<UserProvider>(
                    builder: (context, model, child) {
                      return AvatarWidget(
                        url: model.user?.avatar,
                        size: 30,
                      );
                    },
                  ),
                ),
                const SizedBox(width: 6,),
                Expanded(
                  child: Consumer<LanguageProvider>(
                    builder: (c, language, child) {
                      return Container(
                        color: Colors.transparent,
                        child: Text(
                          I18nLocalizations.of(context).trans('TXT_WRITING_YOUR_STORY'),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 10,),
                      );
                    },
                  ),
                ),
              ],
            ),
            const Divider(
              height: 15,
              indent: 6,
              endIndent: 6,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Align(
                    child: _buildIcon(
                      'ic_camera.svg',
                    ),
                    alignment: Alignment.centerLeft,
                  )
                ),
                const SizedBox(width: 3,),
                _buildIcon(
                  'ic_image.svg',
                ),
                const SizedBox(width: 3,),
                _buildIcon(
                  'ic_location.svg',
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildIcon(String linkImage,) {
    return Container(
      height: 30,
      width: 40,
      padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 8),
      child: SvgPicture.asset(
        'assets/images/icons/$linkImage',
        fit: BoxFit.contain,
        color: Theme.of(context).appBarTheme.iconTheme?.color,
      ),
    );
  }

}
