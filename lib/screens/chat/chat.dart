import 'package:blog/config/config.dart';
import 'package:blog/config/constants.dart';
import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/chat/chat-room.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/screens/chat/chat-room.dart';
import 'package:blog/service/chat-service.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

class ChatScreen extends StatefulWidget {

  @override
  _ChatState createState() => _ChatState();

}

class _ChatState extends State<ChatScreen> {

  TextEditingController _searchController = new TextEditingController();
  List<ChatRoom> liChatRoom = [];
  bool _loading = true;
  StompClient stompClient;

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    Constants.isShowChatScreen = true;
    _searchController.addListener(() {
      // _refresh();
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      connectSocket();
      _refresh();
    });
    super.initState();
  }

  @override
  void dispose() {
    Constants.isShowChatScreen = false;
    stompClient?.deactivate();
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarBuilder.baseAppBar(
        context,
        title: I18nLocalizations.of(context).trans('BTN_CHAT'),
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 20, 10, 20,),
        child: RefreshIndicator(
          child: _buildBody(context),
          backgroundColor: Theme.of(context).backgroundColor,
          onRefresh: _refresh,
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return CustomScrollView(
      slivers: [
        // _buildSearchItem(),
        _buildListChat(),
      ],
    );
  }

  SliverToBoxAdapter _buildSearchItem() {
    return SliverToBoxAdapter(
      child: Container(
        margin: const EdgeInsets.fromLTRB(0, 16, 0, 5),
        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 15,),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Theme.of(context).backgroundColor,
        ),
        child: BasicTextField(
          regexConfig: RegexConstant.none,
          controller: _searchController,
          borderType: BorderType.NONE,
          hintText: I18nLocalizations.of(context).trans('TXT_SEARCH_FRIEND'),
          prefixIcon: Padding(
            padding: const EdgeInsets.only(right: 10,),
            child: Icon(
              Icons.search,
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          ),
        ),
      ),
    );
  }

  _buildListChat() {
    if (liChatRoom.length == 0 && _loading) {
      return SliverToBoxAdapter(
        child: Center(
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 160,),
            height: 32,
            width: 32,
            child: CircularProgressIndicator(strokeWidth: 2.6,),
          ),
        ),
      );
    }
    if (liChatRoom.length == 0)
      return SliverToBoxAdapter(
        child: Center(
          child: EmptyBox(),
        ),
      );
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (c, index) => _buildChatItem(index),
        childCount: liChatRoom.length,
      ),
    );
  }

  Widget _buildChatItem(num index) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (c) => ChatRoomScreen(sender: liChatRoom[index].recipient))
        );
      },
      child: Container(
        padding: const EdgeInsets.only(
          top: 10,
          bottom: 15,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 0.3,
              color: Theme.of(context).textTheme.bodyText2.color.withOpacity(0.1),
            ),
          ),
        ),
        child: Row(
          children: [
            liChatRoom[index].status ?? false
              ? Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(width: 1, color: Theme.of(context).cursorColor,),
                ),
                child: AvatarWidget(
                  url: liChatRoom[index]?.recipient?.avatar,
                  size: 42,
                ),
              )
              : AvatarWidget(
                url: liChatRoom[index]?.recipient?.avatar,
                size: 50,
              ),
            const SizedBox(width: 20,),
            Text(
              liChatRoom[index].recipient.username,
              style: Theme.of(context).textTheme.headline5,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _refresh() async {
    ResponseData responseData = await ChatService.getListChatRoom();
    _loading = false;
    List<ChatRoom> li = responseData?.data ?? [];
    liChatRoom = li;
    setState(() { });
    if (!responseData.status) {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
  }

  void connectSocket() async {
    String token = await SharedPrefsService.getToken();
    stompClient = StompClient(
      config: StompConfig.SockJS(
        url: '${EnvConfig.SOCKET_URL}',
        onConnect: onConnect,
        onWebSocketError: (dynamic error) => print(error.toString()),
        stompConnectHeaders: {'Authorization': 'Bearer $token'},
        webSocketConnectHeaders: {'Authorization': 'Bearer $token'},
      ),
    );
    stompClient.activate();
  }

  void onConnect(StompClient client, StompFrame frame) {
    num userId = Provider.of<UserProvider>(context, listen: false).user.id;
    client.subscribe(
      destination: '/user/$userId/chatroom-list',
      callback: (StompFrame frame) {
        if (frame.body != null) {
          _refresh();
        }
      },
    );
  }

}
