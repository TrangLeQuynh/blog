import 'dart:convert';
import 'package:blog/config/config.dart';
import 'package:blog/config/regex.dart';
import 'package:blog/i18n/localization.dart';
import 'package:blog/models/chat/chat-message.dart';
import 'package:blog/models/common/user.dart';
import 'package:blog/providers/user-provider.dart';
import 'package:blog/screens/chat/widgets/message-item.dart';
import 'package:blog/service/chat-service.dart';
import 'package:blog/utils/shared_prefs_service.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:blog/widgets/items/avatar-user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';


class ChatRoomScreen extends StatefulWidget {

  User sender;
  ChatRoomScreen({ @required this.sender, });

  @override
  _ChatRoomState createState() => _ChatRoomState();

}

class _ChatRoomState extends State<ChatRoomScreen> {

  User user;
  ScrollController _scrollController = new ScrollController();
  TextEditingController _msgController = new TextEditingController();
  List<ChatMessage> liMessage = [];

  StompClient stompClient;


  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      connectSocket();
      setState(() {
        user = Provider.of<UserProvider>(context, listen: false).user;
      });
      ChatService.getListMessage(sender: widget.sender.id,).then((response) {
        List<ChatMessage> li = response.data ?? [];
        liMessage.addAll(li);
        setState(() { });
        _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
      });
    });
    super.initState();
  }

  @override
  void setState(fn) {
    if (this.mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    stompClient?.deactivate();
    super.dispose();
  }

  void connectSocket() async {
    String token = await SharedPrefsService.getToken();
    stompClient = StompClient(
      config: StompConfig.SockJS(
        url: '${EnvConfig.SOCKET_URL}',
        onConnect: onConnect,
        onWebSocketError: (dynamic error) => print(error.toString()),
        stompConnectHeaders: {'Authorization': 'Bearer $token'},
        webSocketConnectHeaders: {'Authorization': 'Bearer $token'},
      ),
    );
    stompClient.activate();
  }

  void onConnect(StompClient client, StompFrame frame) {
    client.subscribe(
      destination: '/user/${user.id}/messages',
      callback: (StompFrame frame) {
        if (frame.body != null) {
          Map<String, dynamic> result = json.decode(frame.body);
          ChatMessage msg = ChatMessage.fromJson(result);
          liMessage.add(msg);
          setState(() { });
          _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
          _seeMessage();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15,),
              child: RefreshIndicator(
                onRefresh: _seeMore,
                backgroundColor: Theme.of(context).backgroundColor,
                child: ListView.builder(
                  itemCount: liMessage.length,
                  itemBuilder: (c, index) {
                    return MessageItem(
                      isSender: widget.sender.id == liMessage[index].senderId,
                      message: '${liMessage[index].content}',
                    );
                  },
                  controller: _scrollController,
                ),
              ),
            )
          ),
          Divider(),
          _buildMessageAction(context),
        ],
      ),
    );
  }

  Widget _buildMessageAction(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20,),
        child: Row(
          children: [
            Expanded(
              child: BasicTextField(
                controller: _msgController,
                hintText: I18nLocalizations.of(context).trans('TXT_WRITE_MESSAGE'),
                borderType: BorderType.NONE,
                regexConfig: RegexConstant.notEmpty,
              ),
            ),
            GestureDetector(
              onTap: () async {
                //add message
                String value = _msgController.text.trim();
                if (value.length == 0) return;
                _sendMessage(value);
                _msgController.clear();
              },
              child: SizedBox(
                width: 32,
                height: 32,
                child: SvgPicture.asset(
                  'assets/images/icons/ic_send_v1.svg',
                  fit: BoxFit.contain,
                  color: Theme.of(context).cursorColor,
                  alignment: Alignment.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _seeMore() async {
    //load more data
  }

  AppBar _buildAppBar() {
    return AppBar(
      toolbarHeight: 50,
      leadingWidth: 56,
      leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          child: Icon(
            Icons.arrow_back_ios_outlined,
            color: Theme.of(context).appBarTheme.iconTheme.color,
          ),
          padding: const EdgeInsets.only(top: 8, bottom: 8, left: 20, right: 0),
        ),
      ),
      title: Row(
        children: [
          AvatarWidget(url: widget.sender?.avatar, size: 36,),
          const SizedBox(width: 10,),
          Text(
            '${widget.sender?.username}',
            style: Theme.of(context).appBarTheme.titleTextStyle,
          ),
        ],
      ),
    );
  }

  void _sendMessage(String msg) {
    ChatMessage chatMessage = new ChatMessage(
      senderId: user.id,
      recipientId: widget.sender.id,
      content: msg,
      senderName: user.username,
    );
    Map<String, dynamic> _formMessage = {
      'senderId': user.id,
      'recipientId': widget.sender.id,
      'content': msg,
      'senderName': user.username,
    };
    stompClient.send(destination: "/app/chat", body: jsonEncode(_formMessage),);
    liMessage.add(chatMessage);
    setState(() { });
    _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
  }

  void _seeMessage() async {
    ChatService.seeMessage(widget.sender.id,);
  }

}
