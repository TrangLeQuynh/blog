import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MessageItem extends StatelessWidget {

  bool isSender;
  String message;

  MessageItem({ @required this.isSender, @required this.message, });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Align(
        alignment: isSender ? Alignment.topLeft : Alignment.topRight,
        child: Container(
          margin: isSender
            ? EdgeInsets.only(right: 100, bottom: 10,)
            : EdgeInsets.only(left: 100, bottom: 10,),
          constraints: BoxConstraints(
            maxWidth: 380,
          ),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10,),
          decoration: BoxDecoration(
            color: isSender ? Theme.of(context).cursorColor : Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Text(
            '$message',
            style: Theme.of(context).textTheme.headline5.copyWith(
              color: isSender ? Colors.white : null,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
