import 'package:blog/models/post/post.dart';
import 'package:blog/models/response/response_data.dart';
import 'package:blog/screens/posts/post-detail.dart';
import 'package:blog/service/post-service.dart';
import 'package:blog/utils/snackbar-builder.dart';
import 'package:blog/widgets/items/drawer-view.dart';
import 'package:blog/screens/posts/post-view.dart';
import 'package:blog/screens/posts/upload-post.dart';
import 'package:blog/widgets/items/empty-box.dart';
import 'package:blog/widgets/items/loadmore-delegate.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {

  @override
  HomePageState createState() => HomePageState();

}

class HomePageState extends State<HomePage> {

  Map<String, dynamic> param = {
    'page' : 0,
    'size' : 12,
  };
  static const double _endReachedThreshold = 100;
  final ScrollController _controller = ScrollController();
  bool _canLoadMore = true;
  bool loading = true;
  List<Post> liPost = [];
  final GlobalKey<ScaffoldState> _homeKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _controller.addListener(_onScroll);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      _refresh();
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onScroll() {
    if(loading || !_canLoadMore)
      return;
    final thresholdReached = _controller.position.extentAfter < _endReachedThreshold;
    if(thresholdReached) {
      load(param['page'] + 1);
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _homeKey,
      appBar: _buildAppBar(context),
      drawer: DrawerView(),
      endDrawerEnableOpenDragGesture: true,
      body: RefreshIndicator(
        onRefresh: _refresh,
        backgroundColor: Theme.of(context).backgroundColor,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 12, 20, 0,),
          child: CustomScrollView(
            controller: _controller,
            physics: AlwaysScrollableScrollPhysics(),
            slivers: [
              SliverToBoxAdapter(
                child: PostUpload(
                  function: () {
                    _refresh();
                  },
                ),
              ),
              loading && liPost.isEmpty
              ? SliverToBoxAdapter(
                child: Center(
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 160,),
                    height: 32,
                    width: 32,
                    child: CircularProgressIndicator(strokeWidth: 2.6,),
                  ),
                ),
              )
              : liPost.length == 0
                ? SliverToBoxAdapter(
                  child: EmptyBox(),
                )
                : _buildListPost(),
              SliverToBoxAdapter(
                child: LoadMoreDelegate(status: _canLoadMore,),
              ),
            ],
          ),
        ),
      )
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      leadingWidth: 100,
      toolbarHeight: 50,
      leading: InkWell(
        onTap: () {
          _homeKey.currentState.openDrawer();
        },
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Icon(
            Icons.menu,
            size: 28,
          ),
          alignment: Alignment.centerLeft,
        ),
      ),
      actions: [
        // InkWell(r
      ],
    );
  }

  Widget _buildListPost() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return PostView(
            post: liPost[index],
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (_) => PostDetailScreen(
                  postId: liPost[index].id,
                  post: liPost[index],
                ))
              ).then((value) {
                _refresh();
              });
            },
          );
        },
        childCount: liPost.length,
      ),
    );
  }

  Future<void> _refresh() async {
    liPost.clear();
    load(0);
  }

  Future<void> load(int page) async {
    setState(() {
      loading = true;
    });
    param['page'] = page;
    ResponseData responseData = await PostService.getListPost(param);
    List<Post> li = responseData.data ?? [];
    if (li.length < param['size']) {
      _canLoadMore = false;
    }
    liPost.addAll(li);
    loading = false;
    setState(() { });
    if (!responseData.status) {
      SnackBarBuilder.showSnackBar(context, message: responseData.message);
    }
    return;
  }

}
